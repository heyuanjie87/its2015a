#pragma once

#include <string>
using namespace std;

#include "sqlite3.h"
#include "dtype.h"

class SqlHelper
{
public:
	SqlHelper(void);
	~SqlHelper(void);

	bool Init(const char *dir, const char *name);

    bool InsertCont(DtRspCont &ct);
	bool InsertWeight(WeightList &wl);
    bool InsertParam(DtRspParam &param);

	bool UpdateStage(int s);
	bool UpdateStatus(WeightList &wl);
	bool UpdataWeight(WeightList &wl);
	bool UpdataSort(WeightList &wl);
	bool UpdateParam(DtRspParam &param);

	bool SearchCont(DtRspCont &ct);
	bool SearchWList(BuyData &bd);
    bool SearchParam(DtRspParam &param);
	bool SortWeightTime(BuyData &bd);

	void Clear();

private:
	void CreateTable(const char *sql);

	void CreateTable();
	void CreateContTable();
	void CreateWeightTable();
	void CreateParamTable();

	void DeleteWeight();
	void DeleteCont();

private:
	static int SearchContCb(void *data, int nColumn, char **colValues, char **colNames);
	static int CountParamCb(void *data, int nColumn, char **colValues, char **colNames);
	static int SearchParamCb(void *data, int nColumn, char **colValues, char **colNames);

private:
	sqlite3 *m_Db;
};

extern SqlHelper theSql;
