#include "StdAfx.h"
#include "WorkParam.h"
#include "DtCov.h"

WorkParam::WorkParam(void)
{
}

WorkParam::~WorkParam(void)
{
}

DtRspCont& WorkParam::GetCont()
{
    return m_ContInfo;
}

void WorkParam::GetTareParam(float &sys, float &down, float &up)
{
	sys = m_InitParam.SystemFixedTare;
	down = m_InitParam.MinTare;
	up = m_InitParam.MaxTare;
}

void WorkParam::GetZeroParam(int &sec, int &devi)
{
	sec = m_InitParam.ZeroTime;
	devi = 0;
}

void WorkParam::Clear(void)
{
	vector <WeightList> tmp;

	tmp.swap(m_BuyData.WList);

	m_BuyData.Stage = 0;
}

void WorkParam::GetValid(string &count, string &weight)
{
	int n;
	WeightList *wl;
    float w = 0;
    int wcnt = 0;
	DtCov cov;

    count = "0";
    weight = "0.0";

    n = (int)m_BuyData.WList.size();
	if (n != 0)
	{
        wl = &m_BuyData.WList[0];

		for (int i = 0; i < n; i ++)
		{
			if (wl[i].Status == WS_DONE)
			{
				wcnt ++;
				w += cov.stof(wl->Weight);
			}
		}

		count = cov.itos(wcnt);
        weight = cov.ftos(w);
	}
}

WListStatus WorkParam::CheckWList()
{
	int rows;
	WeightList *wl;
	WListStatus ret = WLS_NW;
	int delcnt = 0;
	int wcnt = 0;

	rows = (int)m_BuyData.WList.size();
	if (rows == 0)
		return WLS_NULL;

	wl = &m_BuyData.WList[0];

	for (int i = 0; i < rows; i ++)
	{
		if (wl[i].Status == WS_DONE)
		{
			wcnt ++;
		}
		else if (wl[i].Status > WS_DONE)
		{
			delcnt ++;
		}
	}

	if (delcnt == rows)
		ret = WLS_DLE;

	if (wcnt != 0)
	{
		ret = WLS_W;
		if (wcnt + delcnt == rows)
		{
			ret = WLS_WD;
		}
	}

	return ret;
}
