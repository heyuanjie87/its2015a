#include "StdAfx.h"
#include "TbLvEdit.h"

TbLvEdit::TbLvEdit(void)
{
}

TbLvEdit::~TbLvEdit(void)
{

}

bool TbLvEdit::IsDone()
{
    return Completed();
}

BEGIN_MESSAGE_MAP(TbLvEdit, CEdit)
	//{{AFX_MSG_MAP(TbLvEdit)
	ON_WM_CHAR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void TbLvEdit::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags)
{
    wchar_t *lv;

	::CharUpper((LPWSTR)&nChar);

    Input(nChar);
	lv = Get();
	if (lv != NULL)
	{
		this->SetWindowText(CString(lv));
	}
}

void TbLvEdit::Clear()
{
    Reset();
}