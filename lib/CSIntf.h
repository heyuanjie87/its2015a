#pragma once

#include "dtype.h"

#include <xstring>
using namespace std;

class CSIntf
{
public:
	CSIntf(void);
	~CSIntf(void);

	bool GetJobs(DtRspJobs &rsp);
	bool DevLogin(DtReqDevLogin req, DtRspDevLogin &rsp);
	bool GetAcqLine(DtRspAcqLine &rsp);
	bool SetAcqLine(string &line, DtRspAcqLine &rsp);
	bool WorkerLogin(DtWorker &wk, DtRspWkLogin &rsp);
	bool GetInitParam(DtRspParam &rsp);
	bool Call(int mode, DtRspCall &rsp);
	bool SortReport(DtReqSort &req, DtRspSort &rsp);
	bool GetContract(string &cardid, DtRspCont &rsp);
    bool WeightReport(DtReqWeight &req, DtRspWeight &rsp);
    bool Confirm(DtReqConfirm &req, DtRspConfirm &rsp);
	bool EndSort(DtReqEndSort &req, DtRspEndSort &rsp);
	bool Print(DtReqPrint &req, DtRspPrint &rsp);
    bool OperatorConfirm(DtReqOpConfirm &req, DtRspOpConfirm &rsp);
    bool Restore(DtReqRestore &req, DtRspRestore &rsp);
	bool DownloadCache(DtReqDownCache &req, DtRspDownCache &rsp);

public:
    static void SetServerAddr(const wchar_t *ip, int port);
    string GetErr();

private:
	string _err;
	static wstring _sip;
	static int _sport;
};
