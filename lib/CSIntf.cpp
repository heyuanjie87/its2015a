#include "StdAfx.h"
#include "CSIntf.h"

#include "ESocket.h"
#include "DataPkt.h"

int CSIntf::_sport = 5555;
wstring CSIntf::_sip = _T("127.0.0.1");

CSIntf::CSIntf(void)
{
}

CSIntf::~CSIntf(void)
{
}

string CSIntf::GetErr()
{
    return _err;
}

bool CSIntf::DownloadCache(DtReqDownCache &req, DtRspDownCache &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char *rxbuf = NULL;

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.DownloadCacheReq(req, size);

	sock.Send(buf, size);

	rxbuf = new char[4096];
	size = sock.Receive(rxbuf, 4096);

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.DownloadCacheRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();
	delete rxbuf;

	return ret;
}

bool CSIntf::Restore(DtReqRestore &req, DtRspRestore &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char rxbuf[512];

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.RestoreReq(req, size);

	sock.Send(buf, size);

	size = sock.Receive(rxbuf, sizeof(rxbuf));

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.RestoreRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::OperatorConfirm(DtReqOpConfirm &req, DtRspOpConfirm &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char rxbuf[512];

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.OpConfirmReq(req, size);

	sock.Send(buf, size);

	size = sock.Receive(rxbuf, sizeof(rxbuf));

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.OpConfirmRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::GetAcqLine(DtRspAcqLine &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char rxbuf[512];

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.AcqLineGetReq(size);

	sock.Send(buf, size);

	size = sock.Receive(rxbuf, sizeof(rxbuf));

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.AcqLineGetRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::SetAcqLine(string &line, DtRspAcqLine &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char rxbuf[512];

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.AcqLineSetReq(line, size);

	sock.Send(buf, size);

	size = sock.Receive(rxbuf, sizeof(rxbuf));

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.AcqLineSetRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::EndSort(DtReqEndSort &req, DtRspEndSort &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char rxbuf[512];

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.EndSortReq(req, size);

	sock.Send(buf, size);

	size = sock.Receive(rxbuf, sizeof(rxbuf));

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.EndSortRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::GetJobs(DtRspJobs &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char rxbuf[512];

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.JobListReq(size);

	sock.Send(buf, size);

	size = sock.Receive(rxbuf, sizeof(rxbuf));

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.JobListRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::DevLogin(DtReqDevLogin req, DtRspDevLogin &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char rxbuf[512];

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.DevLoginReq(req, size);

	sock.Send(buf, size);
	size = sock.Receive(rxbuf, sizeof(rxbuf));

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.DevLoginRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::WorkerLogin(DtWorker &wk, DtRspWkLogin &rsp)
{
	bool ret = false;
    ESocket sock;
    DataPkt dp;
	UINT8 *buf;
	int size;
	char rxbuf[512];

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.WorkerLoginReq(wk, size);

	sock.Send(buf, size);
    size = sock.Receive(rxbuf, sizeof(rxbuf));

    if (size <= 0)
	{
        _err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.WorkerLoginRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::GetInitParam(DtRspParam &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char *rxbuf = NULL;

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.InitParamReq(size);
    sock.Timeout(2000, 5000);

	sock.Send(buf, size);

    rxbuf = new char[2048];
	size = sock.Receive(rxbuf, 2048);

    if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.InitParamRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();
    delete rxbuf;

	return ret;
}

bool CSIntf::GetContract(string &cardid, DtRspCont &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char *rxbuf = NULL;

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.ContractReq(cardid, size);

	sock.Send(buf, size);

	rxbuf = new char[2048];
	size = sock.Receive(rxbuf, 2048);

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.ContractRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();
	delete rxbuf;

	return ret;
}

bool CSIntf::Call(int mode, DtRspCall &rsp)
{
    bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
    int size;
	DtReqCall req;
	char rxbuf[256];

	req.CallType = mode;

    buf = dp.CallReq(req, size);

	sock.Send(buf, size);
    size = sock.Receive(rxbuf, 256);

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.CallRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();

	return ret;
}

bool CSIntf::SortReport(DtReqSort &req, DtRspSort &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char *rxbuf = NULL;

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.SortReq(req, size);

	sock.Send(buf, size);

	rxbuf = new char[1024];
	size = sock.Receive(rxbuf, 1024);

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.SortRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();
	delete rxbuf;

	return ret;
}

bool CSIntf::WeightReport(DtReqWeight &req, DtRspWeight &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char *rxbuf = NULL;

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.WeightReq(req, size);

	sock.Send(buf, size);

	rxbuf = new char[1024];
	size = sock.Receive(rxbuf, 1024);

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.WeightRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();
	delete rxbuf;

	return ret;
}

bool CSIntf::Confirm(DtReqConfirm &req, DtRspConfirm &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char *rxbuf = NULL;

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	buf = dp.ConfirmReq(req, size);

	sock.Send(buf, size);

	rxbuf = new char[1024];
	size = sock.Receive(rxbuf, 1024);

	if (size <= 0)
	{
		_err = "ͨ��ʧ��";
		goto EXIT;
	}

	ret = dp.ConfirmRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();
	delete rxbuf;

	return ret;
}

bool CSIntf::Print(DtReqPrint &req, DtRspPrint &rsp)
{
	bool ret = false;
	ESocket sock;
	DataPkt dp;
	UINT8 *buf;
	int size;
	char *rxbuf = NULL;

	if (!sock.Create())
		return false;

	if (sock.Connect(_sip.c_str(), _sport) != TRUE)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	sock.Timeout(2000, 10000);

	buf = dp.PrintReq(req, size);

	sock.Send(buf, size);

	rxbuf = new char[2048];
	size = sock.Receive(rxbuf, 2048);

	if (size <= 0)
	{
		_err = "��������ʧ��";
		goto EXIT;
	}

	ret = dp.PrintRsp(rxbuf, size, rsp);

EXIT:
	sock.Close();
	delete rxbuf;

	return ret;
}

void CSIntf::SetServerAddr(const wchar_t *ip, int port)
{
    _sip = ip;
	_sport = port;
}
