#pragma once
#include "dtype.h"

typedef enum
{
    WLS_NULL, //没有列表
	WLS_NW,   //没有称重
	WLS_DLE,  //全删除
    WLS_W,    //有重量
	WLS_WD,   //称重完成   
}WListStatus;

typedef enum
{
    WOS_OK,
	WOS_CONT,
	WOS_TODAY,
}WOverStatus;

class WorkParam
{
public:
	WorkParam(void);
	~WorkParam(void);

	void GetTareParam(float &sys, float &down, float &up);
    WListStatus CheckWList();
	void GetValid(string &count, string &weight);
    void Clear(void);
    void GetZeroParam(int &sec, int &devi);
    DtRspCont& GetCont();

public:
	DtRspParam m_InitParam;
	DtRspCont m_ContInfo;
    BuyData m_BuyData;

private:
	void StringClear(string *s, int n);
};

extern WorkParam theWkParam;
