#pragma once
#include "afxwin.h"
#include "TobLevel.h"

class TbLvEdit : public CEdit, public TobLevel
{

public:
	TbLvEdit(void);
	virtual ~TbLvEdit(void);

protected:
	//{{AFX_MSG(TbLvEdit)
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

public:
	bool IsDone();
	void Clear();
};
