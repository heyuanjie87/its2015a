#include "StdAfx.h"
#include "DataPkt.h"

#define DP_STX_REQ    0xA5
#define DP_STX_RSP    0x5A

string DataPkt::_devsn = "BM123456";

DataPkt::DataPkt(void)
{
}

DataPkt::~DataPkt(void)
{
}

void DataPkt::DevInfoSet(string &DevSn)
{
	_devsn = DevSn;
}

void DataPkt::BufRest()
{
    _buf.resize(sizeof(DtReq) - 1, '0');
	_root["SendDeviceNo"] = _devsn;
}

UINT8* DataPkt::ReqMake(UINT8 cmd, int &size)
{
    FastWriter writer;
	DtReq *req;
	int pos;
	UINT8 *buf;

	_buf += writer.write(_root);
	pos = (int)_buf.size();
    _buf += "00"; //占位，用作校验和结束符

	req = (DtReq*)_buf.data();
	buf = (UINT8 *)req;
	size = pos + 2;

	switch (cmd)
	{
	case DPCMD_WORKER_CHK:
	case DPCMD_PARAM_REQ:
	case DPCMD_CALLNEXT:
	case DPCMD_CALLREPEAT:
	case DPCMD_CONT_INQ:
		req->NeedAck = 0;
		req->NeedData = 1;
		break;
	case DPCMD_CONFIRM:
		req->NeedAck = 0;
		req->NeedData = 0;
		break;
	default:
		req->NeedAck = 0;
		req->NeedData = 1;
		break;
	}

	req->Stx = DP_STX_REQ;
	req->DevId = 0;
	req->Cmd = cmd;
	req->DevType = 1;
	req->MsgId = 1;
	req->Size = OrderReversal(size);
	buf[pos] = 0;
	buf[pos+1] = ChkSum(buf, pos);

    return buf;
}

UINT8* DataPkt::AcqLineGetReq(int &size)
{
	BufRest();

	return ReqMake(DPCMD_ACQLINE_GET, size);
}

bool DataPkt::AcqLineGetRsp(const char *buf, int size, DtRspAcqLine &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;
	Value lines;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_ACQLINE_GET))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

    lines = _root["Lines"];

	for (int i = 0; i < (int)lines.size(); i ++)
	{
        string item;
        
		item = lines[i].asString();
		rsp.Lines.push_back(item);
	}

	rsp.ErrCode = _root["ResultCode"].asInt();
	rsp.ErrMsg  = _root["TipMsg"].asString();

	return ret;
}

int DataPkt::Separate(const char *str, string &dst, int items, string &src)
{
	int spos = 0, epos;
	string *member;
	int i;

	member = &dst;

	for (i = 0; i < items; i ++)
	{
		epos = (int)src.find(str, spos);

		*member = src.substr(spos, epos - spos);
		spos = epos + 1;

		if (spos == 0)
		{
			break;
		}
		member ++;
	}

	return epos;
}

bool DataPkt::Check(DtRsp *rsp, int size, UINT8 cmd)
{
	int dsize;

	//printf("接收: CMD %d, Size %d\n",cmd, size);

	if (rsp == NULL || size < sizeof(DtRsp) - 1)
		return false;

	if (rsp->Stx != DP_STX_RSP)
		return false;

	if (rsp->Cmd != cmd)
		return false;

	dsize = OrderReversal(rsp->Size);

	if (size < dsize)
		return false;

    //printf("Rx %s\n", rsp->Data);
	return true;
}

UINT32 DataPkt::OrderReversal(UINT32 val)
{
	UINT8 *p;
	UINT32 temp;

	p = (UINT8*)&val;
	temp = (p[0] << 24) | (p[1] << 16) | (p[2] << 8) | p[3]; 

	return temp;
}

UINT8 DataPkt::ChkSum(UINT8 *data, UINT32 size)
{
	UINT8 sum = 0;

	while (size --)
	{
		sum += *data;
		data ++;
	}

	return sum;
}

UINT8* DataPkt::JobListReq(int &size)
{
	BufRest();

	return ReqMake(DPCMD_JOBLIST, size);
}

bool DataPkt::JobListRsp(const char *buf, int size, DtRspJobs &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;
	Value jobs;
	DtJob job;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_JOBLIST))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	jobs = _root["Jobs"];
    
	for (int i = 0; i < (int)jobs.size(); i ++)
	{
		Value item;

		item = jobs[i];
        job.Code = item["Code"].asInt();
		job.Name = item["Name"].asString();

		rsp.JobList.push_back(job);
	}

	rsp.ErrCode = _root["ResultCode"].asInt();
	rsp.ErrMsg  = _root["TipMsg"].asString();

	return ret;
}

UINT8* DataPkt::DevLoginReq(DtReqDevLogin &req, int &size)
{
	BufRest();

	_root["ScalesId"] = req.ScalesId;
	_root["StationCode"] = req.StationCode;
	_root["HwVersion"] = req.SwVersion;
	_root["SwVersion"] = req.HwVersion;

	return ReqMake(DPCMD_DEVLOGIN, size);
}

bool DataPkt::DevLoginRsp(const char *buf, int size, DtRspDevLogin &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_DEVLOGIN))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
    rsp.ErrMsg  = _root["TipMsg"].asString();
    rsp.DeviceTime = _root["DeviceTime"].asString();
	rsp.BindState = _root["BindState"].asString();

	return ret;
}

UINT8* DataPkt::WorkerLoginReq(DtWorker &wk, int &size)
{
	BufRest();

	_root["WorkerName"] = wk.Name;
    _root["WorkerCode"] = wk.Id;

	return ReqMake(DPCMD_WORKER_CHK, size);
}

bool DataPkt::WorkerLoginRsp(const char *buf, int size, DtRspWkLogin &rsp)
{
	bool ret = true;
    Reader reader;
    DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_WORKER_CHK))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false; 

    rsp.ErrCode  = _root["ErrCode"].asInt();
    rsp.ErrMsg   = _root["ErrMsg"].asString();
	rsp.JobCode  = _root["JobCode"].asInt();
	rsp.IsFinish = _root["IsFinish"].asBool();

	return ret;
}

UINT8* DataPkt::InitParamReq(int &size)
{
	BufRest();

	return ReqMake(DPCMD_PARAM_REQ, size);
}

bool DataPkt::InitParamRsp(const char *buf, int size, DtRspParam &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_PARAM_REQ))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.ErrMsg  = _root["TipMsg"].asString();
    rsp.CallDelayTime = _root["CallDelayTime"].asString();
	rsp.PoundNum = _root["PoundNum"].asString();
	rsp.SystemFixedTare = _root["SystemFixedTare"].asFloat();
	rsp.MaxTare = _root["MaxTare"].asFloat();
	rsp.MinTare = _root["MinTare"].asFloat();
	rsp.ZeroTime = _root["ZeroTime"].asInt();
	rsp.CardSureMode = _root["CardSureMode"].asString();

	return ret;
}

UINT8* DataPkt::SortReq(DtReqSort &req, int &size)
{
	BufRest();

	_root["LevelCode"] = req.LevelCode;
	_root["BasketNo"] = req.BasketNo;
	_root["RatingTime"] = req.RatingTime;
	_root["SellId"] = req.SaleId;
	_root["InspectorCode"] = req.InspectorCode;
	_root["PoundNum"] = req.PoundNum;
	_root["PoundNoteNum"] = req.PoundNoteNum;

	return ReqMake(DPCMD_SORT_REQ, size);
}

bool DataPkt::SortRsp(const char *buf, int size, DtRspSort &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_SORT_REQ))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.BundingType = _root["BundingType"].asString();
	rsp.ContractType = _root["ContractType"].asString();
	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.ErrMsg = _root["TipMsg"].asString();
	rsp.LevelId = _root["LevelId"].asString();
	rsp.LevelCode = _root["LevelCode"].asString();
	rsp.BasketNo = _root["BasketNo"].asString();

	return ret;
}

UINT8* DataPkt::AcqLineSetReq(string &line, int &size)
{
    BufRest();

	_root["Line"] = line;

    return ReqMake(DPCMD_ACQLINE_SET, size);
}

bool DataPkt::AcqLineSetRsp(const char *buf, int size, DtRspAcqLine &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_ACQLINE_SET))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asInt();
	rsp.ErrMsg = _root["TipMsg"].asString();

	return ret;
}

UINT8* DataPkt::EndSortReq(DtReqEndSort &req, int &size)
{
    BufRest();

	_root["BasketCount"] = req.BasketCount;
    _root["SaleId"] = req.SaleId;

    return ReqMake(DPCMD_ENDSORT, size);
}

bool DataPkt::EndSortRsp(const char *buf, int size, DtRspEndSort &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_ENDSORT))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

    rsp.ErrCode = _root["ResultCode"].asInt();
    rsp.ErrMsg = _root["TipMsg"].asString();

    return ret;
}

UINT8* DataPkt::ConfirmReq(DtReqConfirm &req, int &size)
{
	BufRest();

	_root["SellId"] = req.SellId;
    _root["BankCardNum"] = req.BankCardNum;
    _root["ValidTotalbaskets"] = req.ValidTotalbaskets;
	_root["ValidWeight"] = req.ValidWeight;

	return ReqMake(DPCMD_CONFIRM, size);
}

bool DataPkt::ConfirmRsp(const char *buf, int size, DtRspConfirm &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_CONFIRM))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.ErrMsg = _root["TipMsg"].asString();

	return ret;
}

UINT8* DataPkt::WeightReq(DtReqWeight &req, int &size)
{
    BufRest();

	_root["ScalesId"] = req.ScalesId;
	_root["LevelCode"] = req.LevelCode;
	_root["LevelId"] = req.LevelId;
	_root["BasketNo"] = req.BasketNo;
	_root["Weight"] = req.NetWeight;
	_root["Result"] = req.Result;
	_root["Tare"] = req.Tare;
	_root["WeightTime"] = req.WeightTime;
	_root["RatingTime"] = req.RatingTime;
	_root["WeightState"] = req.WeightState;
    _root["SellId"] = req.SellId;

    return ReqMake(DPCMD_WEIGHT_REQ, size);
}

bool DataPkt::WeightRsp(const char *buf, int size, DtRspWeight &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_WEIGHT_REQ))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.ErrMsg = _root["TipMsg"].asString();

	return ret;
}

UINT8* DataPkt::CallReq(DtReqCall &req, int &size)
{
	BufRest();

	_root["CallType"] = req.CallType;

	return ReqMake(DPCMD_CALLNEXT, size);
}

bool DataPkt::CallRsp(const char *buf, int size, DtRspCall &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_CALLREPEAT) && !Check(pkt, size, DPCMD_CALLNEXT))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asUInt();
	rsp.ErrMsg = _root["TipMsg"].asString();

	return true;
}

UINT8* DataPkt::ContractReq(string &cardid, int &size)
{
	BufRest();

	_root["BankCardNum"] = cardid;

	return ReqMake(DPCMD_CONT_INQ, size);
}

bool DataPkt::ContractRsp(const char *buf, int size, DtRspCont &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_CONT_INQ))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.ErrMsg = _root["TipMsg"].asString();
	rsp.BankCardNum = _root["BankCardNum"].asString();
	rsp.FarmerName = _root["FarmerName"].asString();
	rsp.ContractAmount = _root["ContractAmount"].asString();
	rsp.ContractMargin = _root["ContractMargin"].asString();
	rsp.CurrentMargin = _root["CurrentMargin"].asString();
    rsp.SaleId = _root["SellId"].asString();

	return ret;
}

UINT8* DataPkt::PrintReq(DtReqPrint &req, int &size)
{
    BufRest();

	_root["BankCardNum"] = req.BankCardNum;
	_root["SellId"] = req.SellId;
	_root["PageNum"] = req.PageNum;

    return ReqMake(DPCMD_BUY_UPL, size);
}

bool DataPkt::PrintRsp(const char *buf, int size, DtRspPrint &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_BUY_UPL))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.ErrMsg = _root["TipMsg"].asString();

	return ret;
}

UINT8* DataPkt::OpConfirmReq(DtReqOpConfirm &req, int &size)
{
    BufRest();

	_root["BankCardNum"] = req.BankCardNum;
	_root["SellId"] = req.SellId;

    return ReqMake(DPCMD_BUY_UPL, size);
}

bool DataPkt::OpConfirmRsp(const char *buf, int size, DtRspOpConfirm &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_BUY_UPL))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.ErrMsg = _root["TipMsg"].asString();

	return ret;
}

UINT8* DataPkt::RestoreReq(DtReqRestore &req, int &size)
{
    BufRest();

	_root["SellId"] = req.SellId;

    return ReqMake(DPCMD_RESTORE, size);
}

bool DataPkt::RestoreRsp(const char *buf, int size, DtRspRestore &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_RESTORE))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.ErrMsg = _root["TipMsg"].asString();

	return ret;
}

UINT8* DataPkt::DownloadCacheReq(DtReqDownCache &req, int &size)
{
	BufRest();

	_root["PageNum"] = req.PageNum;
	_root["SaleId"] = req.SaleId;

	return ReqMake(DPCMD_DOWNCACHE, size);
}

bool DataPkt::DownloadCacheRsp(const char *buf, int size, DtRspDownCache &rsp)
{
	bool ret = true;
	Reader reader;
	DtRsp *pkt;

	pkt = (DtRsp *)buf;
	if (!Check(pkt, size, DPCMD_DOWNCACHE))
		return false;

	if (!reader.parse((char*)pkt->Data, _root))
		return false;

	rsp.ErrCode = _root["ResultCode"].asString();
	rsp.TipMsg = _root["TipMsg"].asString();
	rsp.PageNum = _root["TipMsg"].asInt();
	rsp.TotalPage = _root["TipMsg"].asInt();
	rsp.SaleId = _root["TipMsg"].asString();
	rsp.BankCardNum = _root["TipMsg"].asString();
	rsp.SaleState = _root["TipMsg"].asInt();
	rsp.BasketNo = _root["TipMsg"].asInt();
	rsp.LevelId = _root["TipMsg"].asInt();
	rsp.LevelCode = _root["TipMsg"].asString();
	rsp.Weight = _root["TipMsg"].asFloat();
	rsp.Price = _root["TipMsg"].asFloat();
	rsp.Amount = _root["TipMsg"].asFloat();
	rsp.Tare = _root["TipMsg"].asFloat();
	rsp.RattingDate = _root["TipMsg"].asString();
	rsp.WeighingDate = _root["TipMsg"].asString();
	rsp.ContractType = _root["TipMsg"].asString();
	rsp.BundingType = _root["TipMsg"].asString();
	rsp.WeightSate = _root["TipMsg"].asInt();
	rsp.ExcpTip = _root["TipMsg"].asString();

	return ret;
}
