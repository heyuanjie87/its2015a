#pragma once

#include <string>
#include <vector>
using namespace std;

typedef enum
{
	DPCMD_CONT_INQ   = 0x01,
	DPCMD_WORKER_CHK = 0x08,
	DPCMD_PARAM_REQ  = 0x07,
	DPCMD_BUY_UPL    = 0x04,
	DPCMD_CONFIRM    = 0x0D,
	DPCMD_CALLNEXT   = 0x0A,
	DPCMD_CALLREPEAT = 0x0B,
	DPCMD_SORT_REQ   = 0x02, 
	DPCMD_WEIGHT_REQ = 0x0C,
	DPCMD_DEVLOGIN   = 15,
	DPCMD_JOBLIST    = 14,
    DPCMD_ENDSORT    = 0x14,
    DPCMD_ACQLINE_GET = 0x13,
    DPCMD_ACQLINE_SET = 0x10,
    DPCMD_PRINT = 0x16,
	DPCMD_RESTORE = 0x15,
	DPCMD_DOWNCACHE = 0x11,
} DPCMD;

typedef enum
{
	WS_NONE = 0,
	WS_SORT,     //定级完成

	WS_DONE,     //称重完成
	WS_EXCEP,    //定级异常(算完成)
	WS_DELETED,  //已删除(完成)
}WeightStat;

#pragma pack(1)
typedef struct
{
	UINT8 Stx;
    UINT32 Size;
	UINT8 MsgId;
	UINT8 DevType;
	UINT8 NeedAck;
	UINT8 NeedData;
	UINT8 Cmd;
	UINT8 DevId;
	UINT8 Data[1];
//  UINT8 Stat;
}DtReq;

//数据应答包头定义
typedef struct
{
	UINT8 Stx;
    UINT32 Size;
	UINT8 MsgId;
	UINT8 DevType;
	UINT8 Status;
	UINT8 Cmd;
	UINT8 DevId;
	UINT8 Data[1];
//  UINT8 Stat;
} DtRsp;
#pragma pack()

typedef struct  
{
    string Name;
	string Id;
}DtWorker;

typedef struct
{
    string SaleId;
	string SortSn;
	string LevelId;
    string LevelCode;
	string Weight;
	string UnitPrice;
	string TotalPrice;
	string Tare;
	string SortTime;
	string WeightTime;
	string ContType;
	string PackMode;
	string ExcepTip;
	WeightStat Status;
} WeightList;

typedef struct  
{
	string ScalesId;
	string StationCode;
	string HwVersion;
	string SwVersion;
}DtReqDevLogin;

typedef struct
{
    string ErrCode;
	string ErrMsg;
	string DeviceTime;
	string SwVersion;
	string BindState;
}DtRspDevLogin;

typedef struct  
{
	string ErrCode;
	string ErrMsg;
	vector <string> Lines;
}DtRspAcqLine;

typedef struct  
{
	int Code;
	string Name;
}DtJob;

typedef struct  
{
    string ErrCode;
	string ErrMsg;
    vector<DtJob> JobList;
}DtRspJobs;

typedef struct  
{
	string ErrCode;
	string ErrMsg;
	int JobCode;
	bool IsFinish;
}DtRspWkLogin;

typedef struct  
{
	string ErrCode;
	string ErrMsg;
	string StationCode;
	string CallDelayTime;
	string PoundNum;
	float SystemFixedTare;
	float MaxTare;
	float MinTare;
	int ZeroTime;
	string ScaleChk;
	string ScaleId;
	string CardSureMode;
}DtRspParam;

typedef struct  
{
	string BasketNo;
    string LevelCode;
    string RatingTime;
	string SaleId;
	string InspectorCode;
	string PoundNum;
	string PoundNoteNum;
}DtReqSort;

typedef struct  
{
	string ErrCode;
	string ErrMsg;
	string BasketNo;
	string LevelId;
	string LevelCode;
	string ContractType;
	string BundingType;
}DtRspSort;

typedef struct 
{
	string ErrCode;
	string ErrMsg;
	string BankCardNum;
    string FarmerName;
	string ContractAmount;
	string ContractMargin;
	string CurrentMargin;
	string VarietiesName;
    string SaleId;

	int Stage;
}DtRspCont;

typedef struct  
{
    string ScalesId;
	string Result;
	string BasketNo;
	string LevelId;
	string LevelCode;
	string Tare;
	string NetWeight;
	string WeightState;
    string WeightTime;
	string RatingTime;
	string SellId;
}DtReqWeight;

typedef struct  
{
	string ErrCode;
	string ErrMsg;
}DtRspWeight;

typedef struct  
{
	string SellId;
	string BankCardNum;
	string ValidTotalbaskets;
	string ValidWeight;
}DtReqConfirm;

typedef struct  
{
    string ErrCode;
	string ErrMsg;
}DtRspConfirm;

typedef struct  
{
	string SaleId;
    string BasketCount;
}DtReqEndSort;

typedef struct  
{
    string ErrCode;
	string ErrMsg;
}DtRspEndSort;

typedef struct  
{
	int CallType;
}DtReqCall;

typedef struct  
{
    string ErrCode;
	string ErrMsg;
}DtRspCall;

typedef struct  
{
	string SellId;
	string BankCardNum;
	int PageNum;
}DtReqPrint;

typedef struct  
{
    string ErrCode;
	string ErrMsg;
	string PageNum;
	string TotalPage;
}DtRspPrint;

typedef struct  
{
	string SellId;
	string BankCardNum;
}DtReqOpConfirm;

typedef struct  
{
    string ErrCode;
	string ErrMsg;
}DtRspOpConfirm;

typedef struct
{
	string SellId;
}DtReqRestore;

typedef struct  
{
	string ErrCode;
	string ErrMsg;
}DtRspRestore;

typedef struct  
{
    int PageNum;
	string SaleId;
}DtReqDownCache;

typedef struct  
{
    string ErrCode;
	string TipMsg;
    int PageNum;
	int TotalPage;
	string SaleId;
	string BankCardNum;
	int SaleState;
    int BasketNo;
	int LevelId;
	string LevelCode;
	float Weight;
	float Price;
	float Amount;
	float Tare;
	string RattingDate;
	string WeighingDate;
	string ContractType;
	string BundingType;
	int WeightSate;
	string ExcpTip;
}DtRspDownCache;

typedef struct
{
    int Stage; //业务进度，不上传

	vector <WeightList> WList;
} BuyData;
