#pragma once

#include "dtype.h"

#include <string>
using namespace std;

#include "json/json.h"
using namespace Json;

class DataPkt
{
public:
	DataPkt(void);
	~DataPkt(void);

	static void DevInfoSet(string &DevSn);

	UINT8* DevLoginReq(DtReqDevLogin &req, int &size);
    bool DevLoginRsp(const char *buf, int size, DtRspDevLogin &rsp);

	UINT8* AcqLineSetReq(string &line, int &size);
	bool AcqLineSetRsp(const char *buf, int size, DtRspAcqLine &rsp);

	UINT8* AcqLineGetReq(int &size);
	bool AcqLineGetRsp(const char *buf, int size, DtRspAcqLine &rsp);

	UINT8* JobListReq(int &size);
	bool JobListRsp(const char *buf, int size, DtRspJobs &rsp);

	UINT8* WorkerLoginReq(DtWorker &wk, int &size);
    bool WorkerLoginRsp(const char *buf, int size, DtRspWkLogin &rsp);
    
	UINT8* InitParamReq(int &size);
	bool InitParamRsp(const char *buf, int size, DtRspParam &rsp);

	UINT8* SortReq(DtReqSort&req, int &size);
	bool SortRsp(const char *buf, int size, DtRspSort &rsp);

	UINT8* CallReq(DtReqCall &req, int &size);
	bool CallRsp(const char *buf, int size, DtRspCall &rsp);

	UINT8* ContractReq(string &cardid, int &size);
	bool ContractRsp(const char *buf, int size, DtRspCont &rsp);

	UINT8* WeightReq(DtReqWeight &req, int &size);
	bool WeightRsp(const char *buf, int size, DtRspWeight &rsp);

	UINT8* EndSortReq(DtReqEndSort &req, int &size);
	bool EndSortRsp(const char *buf, int size, DtRspEndSort &rsp);

	UINT8* ConfirmReq(DtReqConfirm &req, int &size);
	bool ConfirmRsp(const char *buf, int size, DtRspConfirm &rsp);

	UINT8* PrintReq(DtReqPrint &req, int &size);
	bool PrintRsp(const char *buf, int size, DtRspPrint &rsp);

	UINT8* OpConfirmReq(DtReqOpConfirm &req, int &size);
	bool OpConfirmRsp(const char *buf, int size, DtRspOpConfirm &rsp);

	UINT8* RestoreReq(DtReqRestore &req, int &size);
	bool RestoreRsp(const char *buf, int size, DtRspRestore &rsp);

	UINT8* DownloadCacheReq(DtReqDownCache &req, int &size);
	bool DownloadCacheRsp(const char *buf, int size, DtRspDownCache &rsp);

private:
	void BufRest();
    UINT8* ReqMake(UINT8 cmd, int &size);
    UINT8 ChkSum(UINT8 *data, UINT32 size);
    UINT32 OrderReversal(UINT32 val);
    bool Check(DtRsp *rsp, int size, UINT8 cmd);
    int Separate(const char *str, string &dst, int items, string &src);

private:
	Value _root;
	string _buf;
	static string _devsn;
};
