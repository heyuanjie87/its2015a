#include "StdAfx.h"
#include "SqlHelper.h"
#include "DtCov.h"
#include <stdio.h>
#include <direct.h>

#include <sstream>
#include <iostream>
using namespace std;

SqlHelper::SqlHelper(void)
{
	m_Db  = NULL;
}

SqlHelper::~SqlHelper(void)
{
}

bool SqlHelper::Init(const char *dir, const char *name)
{
	int state_s = 0;// 0:数据库已经存在 1:数据库不存在 
	FILE *stream;
    string path;
    DtCov cov;

	mkdir(dir);

	path = dir;
	path += name;
    path = cov.ctoutf8(path);

	if ((stream = fopen(path.c_str(), "r")) == NULL)
	{
		state_s = 1;
	}
	else
	{
		fclose(stream);
	}

	int rc = sqlite3_open(path.c_str(), &m_Db);
	if (rc)
	{
		m_Db = NULL;
		return false;
	}

	if (state_s)
	{
		CreateTable();
	}

	return true;
}

void SqlHelper::CreateTable(const char *sql)
{
	char *zErrMsg;

	if (sqlite3_exec(m_Db, sql, 0, 0, &zErrMsg))
	{
		sqlite3_free( zErrMsg );
	}
}

void SqlHelper::CreateWeightTable()
{
	char tlb2[] = "CREATE TABLE  WeightList"
		          "("
                  "SaleId VARCHAR,"
				  "SortSn VARCHAR,"
				  "LevelId VARCHAR,"
                  "LevelCode VARCHAR,"
				  "Weight VARCHAR,"
				  "UnitPrice VARCHAR,"
				  "TotalPrice VARCHAR,"
				  "Tare VARCHAR,"
				  "SortTime VARCHAR,"
				  "WeightTime VARCHAR,"
				  "ContType VARCHAR,"
				  "PackMode VARCHAR,"
				  "WeightState INTEGER,"
				  "ExcepTip VARCHAR"
				  ");";
 
    CreateTable(tlb2);
}

void SqlHelper::CreateParamTable()
{
	char tlb[] = "CREATE TABLE  Param"
		"("
		"Station VARCHAR"
		");";

	CreateTable(tlb);
}

void SqlHelper::CreateContTable()
{
	char tlb[] = "CREATE TABLE Contract"
		"("
		"BankCardNum VARCHAR,"
		"FarmerName VARCHAR,"
		"ContractAmount VARCHAR,"
		"ContractMargin VARCHAR,"
		"CurrentMargin VARCHAR,"
		"VarietiesName VARCHAR,"
		"SaleId VARCHAR,"
        "Stage VARCHAR"
		");";

    CreateTable(tlb);
}

void SqlHelper::CreateTable()
{
	CreateParamTable();
    CreateContTable();
    CreateWeightTable();
}

bool SqlHelper::UpdateStage(int stage)
{
	string head = "UPDATE Contract SET Stage = ";

	ostringstream temp;

	temp << head
		 << stage;

	const string &s = temp.str().c_str();

	if (sqlite3_exec(m_Db, s.c_str(), NULL, NULL, 0) != SQLITE_OK) 
	{
		return false;
	}

	return true;
}

bool SqlHelper::UpdateParam(DtRspParam &param)
{
	string head = "UPDATE Param SET ";

	ostringstream temp;

	temp << head
		 << "Station = '" << param.StationCode << "'";

	const string &s = temp.str().c_str();

	if (sqlite3_exec(m_Db, s.c_str(), NULL, NULL, 0) != SQLITE_OK) 
	{
		return false;
	}

	return true;
}

bool SqlHelper::InsertCont(DtRspCont &cont)
{
	string head = "INSERT INTO Contract VALUES(";

	ostringstream temp;

	temp << head
		 << "'" << cont.Stage          << "',"
		 << "'" << cont.BankCardNum    << "',"
		 << "'" << cont.FarmerName     << "',"
		 << "'" << cont.ContractAmount << "',"
		 << "'" << cont.ContractMargin << "',"
		 << "'" << cont.CurrentMargin  << "',"
		 << "'" << cont.VarietiesName  << "',"
		 << "'" << cont.SaleId         << "');";

	const string& s = temp.str().c_str();

	if (sqlite3_exec(m_Db, s.c_str(), NULL, NULL, 0) != SQLITE_OK) 
	{
		return false;
	}

	return true;
}

int SqlHelper::CountParamCb(void *data, int nColumn, char **colValues, char **colNames)
{
    *((int*)data) = atoi(colValues[0]);

	return 0;
}

bool SqlHelper::InsertParam(DtRspParam &param)
{
	ostringstream temp;
	string head = "SELECT COUNT(*) FROM Param";
    int cnt = 0;

	if (sqlite3_exec(m_Db, head.c_str(), CountParamCb, &cnt, 0) != SQLITE_OK) 
	{
		return false;
	}

	if (cnt != 0)
	{
		return true;
	}

    head = "INSERT INTO Param VALUES(";
	temp << head
		<< "'" << param.StationCode        << "');";

	const string& s = temp.str().c_str();

	if (sqlite3_exec(m_Db, s.c_str(), NULL, NULL, 0) != SQLITE_OK) 
	{
		return false;
	}

	return true;
}

int SqlHelper::SearchContCb(void *data, int nColumn, char **colValues, char **colNames)
{
    DtRspCont *ct;
    int index = 0;

	ct = (DtRspCont*)data;

	ct->BankCardNum    = colValues[index++];
	ct->FarmerName     = colValues[index++];
	ct->ContractAmount = colValues[index++];
	ct->ContractMargin = colValues[index++];
	ct->CurrentMargin  = colValues[index++];
	ct->VarietiesName  = colValues[index++];
	ct->SaleId         = colValues[index++];
	ct->Stage          = atoi(colValues[index++]);

	return 0;
}

bool SqlHelper::SearchCont(DtRspCont &ct)
{
	ostringstream temp;

	temp << "SELECT * FROM Contract;";

	if (sqlite3_exec(m_Db, temp.str().c_str(), SearchContCb, &ct, 0)!=SQLITE_OK)
	{
		return false;
	}

	return true;
}

static int SearchWListCb(void *data, int nColumn, char **colValues, char **colNames)
{
	BuyData *bd;
    WeightList wl;
	int index = 0;

	bd = (BuyData *)data;

	wl.SaleId     = colValues[index++];
    wl.SortSn     = colValues[index++];
	wl.LevelId    = colValues[index++];
	wl.LevelCode  = colValues[index++];
	wl.Weight     = colValues[index++];
	wl.UnitPrice  = colValues[index++];
	wl.TotalPrice = colValues[index++];
	wl.Tare       = colValues[index++];
	wl.SortTime   = colValues[index++];
	wl.WeightTime = colValues[index++];
	wl.ContType   = colValues[index++];
	wl.PackMode   = colValues[index++];
	wl.Status     = (WeightStat)atoi(colValues[index++]);
    wl.ExcepTip   = colValues[index++];
    
    bd->WList.push_back(wl);

	return 0;
}

bool SqlHelper::SearchWList(BuyData &bd)
{
	ostringstream temp;

	temp << "SELECT * FROM WeightList;";

	if (sqlite3_exec(m_Db, temp.str().c_str(), SearchWListCb, &bd, 0)!=SQLITE_OK)
	{
		return false;
	}

	return true;
}

int SqlHelper::SearchParamCb(void *data, int nColumn, char **colValues, char **colNames)
{
    DtRspParam *param;

	param = (DtRspParam *)data;

	param->StationCode = colValues[0];

	return 0;
}

bool SqlHelper::SearchParam(DtRspParam &param)
{
	ostringstream temp;

	temp << "SELECT * FROM Param;";

	if (sqlite3_exec(m_Db, temp.str().c_str(), SearchParamCb, &param, 0)!=SQLITE_OK)
	{
		return false;
	}

	return true;
}

bool SqlHelper::SortWeightTime(BuyData &bd)
{
	string head = "SELECT * FROM WeightList ORDER BY WeightTime ASC;";

	if (sqlite3_exec(m_Db, head.c_str(), SearchWListCb, &bd, 0) != SQLITE_OK)
	{
		return false;
	}

	return true;
}

bool SqlHelper::InsertWeight(WeightList &wl)
{
	string head = "INSERT INTO WeightList VALUES(";

	ostringstream temp;

    temp << head
		 << "'" << wl.SaleId      << "',"
         << "'" << wl.SortSn      << "',"
		 << "'" << wl.LevelId     << "',"
		 << "'" << wl.LevelCode   << "',"
		 << "'" << wl.Weight      << "',"
		 << "'" << wl.UnitPrice   << "',"
		 << "'" << wl.TotalPrice  << "',"
		 << "'" << wl.Tare        << "',"
		 << "'" << wl.SortTime    << "',"
		 << "'" << wl.WeightTime  << "',"
		 << "'" << wl.ContType    << "',"
		 << "'" << wl.PackMode    << "',"
		 << "'" << wl.Status      << "',"
		 << "'" << wl.ExcepTip    << "');";

	const string& s = temp.str().c_str();

	if (sqlite3_exec(m_Db, s.c_str(), NULL, NULL, 0) != SQLITE_OK) 
	{
		return false;
	}

	return true;
}

bool SqlHelper::UpdateStatus(WeightList &wl)
{
	string head = "UPDATE WeightList SET Status = ";

	ostringstream temp;

	temp << head
		 << wl.Status
		 << " WHERE SortSn = " << wl.SortSn;

	const string &s = temp.str().c_str();

	if (sqlite3_exec(m_Db, s.c_str(), NULL, NULL, 0) != SQLITE_OK) 
	{
		return false;
	}

	return true;
}

bool SqlHelper::UpdataWeight(WeightList &wl)
{
	string head = "UPDATE WeightList SET ";

	ostringstream temp;

	temp << head
		 << "Weight = '"     << wl.Weight << "',"
		 << "Tare = '"       << wl.Tare   << "',"
		 << "TotalPrice = '" << wl.TotalPrice << "',"
         << "WeightTime = '" << wl.WeightTime << "',"
		 << "Status = "     << wl.Status 
	     << " WHERE SortSn = " << wl.SortSn;

	const string &s = temp.str().c_str();

	if (sqlite3_exec(m_Db, s.c_str(), NULL, NULL, 0) != SQLITE_OK) 
	{
		return false;
	}

	return true;
}

bool SqlHelper::UpdataSort(WeightList &wl)
{
	string head = "UPDATE WeightList SET ";

	ostringstream temp;

	temp << head
		<< "PackMode = '"      << wl.Weight     << "',"
		<< "ContMode = '"      << wl.Tare       << "',"
		<< "UnitPrice = '"     << wl.TotalPrice << "',"
		<< "Status = "        << wl.Status 
		<< " WHERE SortSn = " << wl.SortSn;

	const string &s = temp.str().c_str();

	if (sqlite3_exec(m_Db, s.c_str(), NULL, NULL, 0) != SQLITE_OK) 
	{
		return false;
	}

	return true;
}

void SqlHelper::DeleteWeight()
{
	char *bdWList = "DELETE FROM WeightList;";

	sqlite3_exec(m_Db, bdWList, 0, 0, 0);
}

void SqlHelper::DeleteCont()
{
	char *cont = "DELETE FROM Contract;";

	sqlite3_exec(m_Db, cont, 0, 0, 0);
}

void SqlHelper::Clear()
{
    DeleteWeight();
    DeleteCont();
}
