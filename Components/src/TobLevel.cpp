#include "StdAfx.h"
#include "TobLevel.h"
#include "Keyboard.h"

typedef enum 
{
	LEVEL_KEY_BK  = KEY_11,
	LEVEL_KEY_BL  = KEY_12,
	LEVEL_KEY_BF  = KEY_13,

	LEVEL_KEY_CXK = KEY_21,
	LEVEL_KEY_CL  = KEY_22,
	LEVEL_KEY_CF  = KEY_23,

	LEVEL_KEY_HF  = KEY_31,
	LEVEL_KEY_XL  = KEY_32,
	LEVEL_KEY_XF  = KEY_33,

	LEVEL_KEY_S   = KEY_41,
	LEVEL_KEY_GY  = KEY_42,
	LEVEL_KEY_BR  = KEY_43,

	LEVEL_KEY_X2V = KEY_51,
	LEVEL_KEY_C3V = KEY_52,
	LEVEL_KEY_BV  = KEY_53,
	LEVEL_KEY_JW  = KEY_61,
}eLevelKey;

static wchar_t *LevelName[] =
{
	L"",
	// 1
	L"C1F",
	// 2
	L"C2F",
	// 3
	L"C3F",
	// 4
	L"C1L",
	// 5
	L"C2L",
	// 6
	L"B1F",
	// 7
	L"B2F",
	// 8
	L"B1L",
	// 9
	L"B1R",
	// 10
	L"H1F",
	// 11
	L"X1F",
	// 12
	L"C3L",
	// 13
	L"C4F",
	// 14
	L"C4L",
	// 15
	L"X2F",
	// 16
	L"X3F",
	// 17
	L"X1L",
	// 18
	L"X2L",
	// 19
	L"B3F",
	// 20
	L"B4F",
	// 21
	L"B2L",
	// 22
	L"B3L",
	// 23
	L"B2R",
	// 24
	L"B3R",
	// 25
	L"H2F",
	// 26
	L"X2V",
	// 27
	L"C3V",
	// 28
	L"B2V",
	// 29
	L"B3V",
	// 30
	L"S1",
	// 31
	L"B4L",
	// 32
	L"X3L",
	// 33
	L"X4L",
	// 34
	L"X4F",
	// 35
	L"S2",
	// 36
	L"CX1K",
	// 37
	L"CX2K",
	// 38
	L"B1K",
	// 39
	L"B2K",
	// 40
	L"B3K",
	// 41
	L"GY1",
	// 42
	L"GY2",
	// 43
	L"JW",
};

TobLevel::TobLevel(void)
{
	m_LvId = 0;
    Reset();
}

TobLevel::~TobLevel(void)
{
}

struct KeyLevelPrompt //按键、等级及提示
{
	wchar_t* name;
	BYTE mlk;
	BYTE numlv;
	BYTE level[4];
};

static KeyLevelPrompt _prompt[] =
{
	{ L"C?F", LEVEL_KEY_CF, 4, LEVEL_C1F, LEVEL_C2F, LEVEL_C3F, LEVEL_C4F },
	{ L"C?L", LEVEL_KEY_CL, 4, LEVEL_C1L, LEVEL_C2L, LEVEL_C3L, LEVEL_C4L },
	{ L"B?F", LEVEL_KEY_BF, 4, LEVEL_B1F, LEVEL_B2F, LEVEL_B3F, LEVEL_B4F },
	{ L"B?L", LEVEL_KEY_BL, 4, LEVEL_B1L, LEVEL_B2L, LEVEL_B3L, LEVEL_B4L },
	{ L"X?F", LEVEL_KEY_XF, 4, LEVEL_X1F, LEVEL_X2F, LEVEL_X3F, LEVEL_X4F },
	{ L"X?L", LEVEL_KEY_XL, 4, LEVEL_X1L, LEVEL_X2L, LEVEL_X3L, LEVEL_X4L },
	{ L"B?V", LEVEL_KEY_BV, 2, LEVEL_NULL, LEVEL_B2V, LEVEL_B3V, LEVEL_NULL },
    { L"B?K", LEVEL_KEY_BK, 3, LEVEL_B1K, LEVEL_B2K, LEVEL_B3K, LEVEL_NULL },
	{ L"B?R", LEVEL_KEY_BR,   3, LEVEL_B1R, LEVEL_B2R, LEVEL_B3R, LEVEL_NULL },
	{ L"C3V", LEVEL_KEY_C3V,  1, LEVEL_C3V, LEVEL_NULL, LEVEL_NULL, LEVEL_NULL },
	{ L"CX?K", LEVEL_KEY_CXK, 2, LEVEL_CX1K, LEVEL_CX2K, LEVEL_NULL, LEVEL_NULL },
	{ L"X2V", LEVEL_KEY_X2V,  1, LEVEL_X2V, LEVEL_NULL, LEVEL_NULL, LEVEL_NULL },
	{ L"S?",  LEVEL_KEY_S,  2, LEVEL_S1, LEVEL_S2, LEVEL_NULL, LEVEL_NULL },
	{ L"GY?", LEVEL_KEY_GY, 2, LEVEL_GY1, LEVEL_GY2, LEVEL_NULL, LEVEL_NULL },
	{ L"H?F", LEVEL_KEY_HF, 2, LEVEL_H1F, LEVEL_H2F, LEVEL_NULL, LEVEL_NULL },
	{ L"JW",  LEVEL_KEY_JW, 1, LEVEL_JW, LEVEL_NULL, LEVEL_NULL, LEVEL_NULL },
};

wchar_t* TobLevel::Get()
{
    return m_name;
}

wchar_t* TobLevel::Get(string &id)
{
    int n;

	n = atoi(id.c_str());
	if (n > LEVEL_NULL && n < LEVEL_MAX)
	{
		return LevelName[n];
	}

	return NULL;
}

bool TobLevel::Completed()
{
    return m_Comp;
}

void TobLevel::Reset()
{
	m_NumKey = 0;
	m_Index = 0;
	m_Comp = false;
	m_name = LevelName[0];
}

bool TobLevel::IsLevelKey(int key)
{
	bool ret = false;

	for (int i = 0; i < _countof(_prompt); i ++)
	{
		if (key == _prompt[i].mlk)
		{
            ret = true;
			break;
		}
	}

	return ret;
}

void TobLevel::Input(int key)
{
	if (key == KEY_DELETE)
	{
		m_Comp = false;
		m_NumKey = 0;
		m_Index = 0;
        m_name = LevelName[0];
		goto EXIT;
	}

	if (key == '.')
		key = LEVEL_KEY_JW;

	if (m_NumKey == 0)
	{
		for (int i = 0; i < _countof(_prompt); i ++)
		{
			if (key == _prompt[i].mlk)
			{
				m_Comp   = false;
				m_NumKey = 1;
				m_Index  = i;
				m_name   = _prompt[i].name;

				if (_prompt[i].numlv == 1)
				{
					m_LvId = _prompt[m_Index].level[0];
                    goto COMP;
				}

                break;
			}	
		}

        goto EXIT;
	}
	else
	{
        key -= '1';

		if ((key < 0) || (key > 3) || (_prompt[m_Index].level[key] == LEVEL_NULL))
		{
            m_name = _prompt[m_Index].name;
			goto EXIT;
		}

		m_LvId = _prompt[m_Index].level[key];

		m_name = LevelName[m_LvId];
	}

COMP:
    m_Comp = true;
	m_NumKey = 0;

EXIT:
	return;
}

int TobLevel::GetId(wchar_t *nm)
{
	if (wcscmp(nm, m_name) == 0)
	{
		return m_LvId;
	}
	else
	{
		for (int i = 1; i < _countof(LevelName); i ++)
		{
            if (wcscmp(LevelName[i], nm) == 0)
			{
				return i;
			}
		}
	}

    return 0;
}

void TobLevel::GetId(wchar_t *nm, string &id)
{
	int n = 0;
    char buf[12];

	if (wcscmp(nm, m_name) == 0)
	{
		n = m_LvId;
	}
	else
	{
		for (int i = 1; i < _countof(LevelName); i ++)
		{
			if (wcscmp(LevelName[i], nm) == 0)
			{
				n = i;
				break;
			}
		}
	}
    
    _itoa(n, buf, 10);
	id = buf;
}
