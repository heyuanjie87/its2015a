#include "StdAfx.h"
#include "SoundPlay.h"

#ifndef UNDER_CE
#include <mmsystem.h>
#pragma comment(lib, "WINMM.LIB")
#endif

SoundPlay::SoundPlay(void)
{
}

SoundPlay::~SoundPlay(void)
{
}

void SoundPlay::clear()
{
	while (!sndFiles.empty() )
		sndFiles.pop();
}

void SoundPlay::playSound( const wchar_t* name )
{
	wchar_t buf[ MAX_PATH ];
	swprintf( buf,  L"%s.wav", name );
	playFile( buf );
}

void SoundPlay::playFile(const wstring &name)
{
	sndFiles.push( name );
}

void SoundPlay::playAsync( const std::wstring& name )
{
	std::wstring full = soundPath + name;
	sndPlaySound( full.c_str(), SND_ASYNC );
}

bool SoundPlay::getNextSoundFile( std::wstring& file )
{
	if ( sndFiles.empty() )
		return false;

	file = sndFiles.front();
	sndFiles.pop();

	return true;
}

void SoundPlay::Open()
{
	Start();
}

void SoundPlay::Close()
{
    isRunning = false;
}

void SoundPlay::PlayTips(WCHAR *s)
{
    wstring dir = _T("�չ�����/");

	PlayFile(dir, s);
}

void SoundPlay::PlayWeight(wstring &lv, float w)
{
    PlayLevel(lv);
    PlayNumber(w);
}

void SoundPlay::PlayWeight(string &lv, float w)
{
	WCHAR buf[20];
    int i;

	for (i = 0; i < lv.length(); i ++)
	{
        buf[i] = lv.at(i);
	}
	buf[i] = 0;

	PlayLevel(wstring(buf));
	PlayNumber(w);
}

void SoundPlay::PlayLevel(wstring &lv)
{
    wstring dir;

	dir = _T("�ȼ�����/DJ_");
    PlayFile(dir, (WCHAR*)lv.c_str());
}

void SoundPlay::PlayNumber(int val)
{
    char buf[32];
    int digit;
	wstring dir;
	int n;
	int i;

	_itoa(val, buf, 10);
	digit = strlen(buf);
    dir = _T("��������/");

    for (n = digit, i = 0; n > 0; n --, i ++)
	{
        WCHAR tmp[2];

        tmp[0] = buf[i];
		tmp[1] = 0;
        
		if (digit != 1 && n == 1)
		{
            if (tmp[0] == '0')
				break;
		}

		PlayFile(dir, tmp);

		switch(n)
		{
		case 5:
            PlayFile(dir, _T("Wian"));
			break;
		case 4:
			PlayFile(dir, _T("Qian"));
			break;
		case 3:
			PlayFile(dir, _T("Bai"));
			break;
		case 2:
			PlayFile(dir, _T("Shi"));
			break;
		case 1:
			break;
		}
	}

	PlayFile(dir, _T("GONGJ"));
}

void SoundPlay::PlayNumber(float val)
{
	char buf[32];
	int digit;
	wstring dir;
	int n;
	int i;

	string num;
	string intpart;
	string decpart;

	dir = _T("��������/");
    
	sprintf(buf, "%.2f", val);
	digit = strlen(buf) - 3;

	for (n = digit, i = 0; n > 0; n --, i ++)
	{
		WCHAR tmp[2];

		tmp[0] = buf[i];
		tmp[1] = 0;

		if (digit != 1 && n == 1)
		{
			if (tmp[0] == '0')
				break;
		}

		PlayFile(dir, tmp);

		switch(n)
		{
		case 5:
			PlayFile(dir, _T("Wian"));
			break;
		case 4:
			PlayFile(dir, _T("Qian"));
			break;
		case 3:
			PlayFile(dir, _T("Bai"));
			break;
		case 2:
			PlayFile(dir, _T("Shi"));
			break;
		case 1:
			break;
		}
	}

    PlayFile(dir, _T("Dian"));

	for (n = digit + 1, i = 0; i < 2; n ++, i ++)
	{
		WCHAR tmp[2];

		tmp[0] = buf[n];
		tmp[1] = 0;
	
	    PlayFile(dir, tmp);
	}

	PlayFile(dir, _T("GONGJ"));
}

void SoundPlay::PlayFile(wstring &path, WCHAR *name)
{
    wstring file;
    
	file = path + name;
	file += _T(".wav");

    sndFiles.push(file);
}

int SoundPlay::Run()
{
	isRunning = true;

	while (isRunning)
	{
		wstring file;
		
		if (getNextSoundFile(file))
		{
			if ( file.c_str()[0] == '*' )
			{
				wstring::size_type pos = file.find(' ');
				
				if ( pos != wstring::npos )
				{
				    wstring str = file.substr( pos );
					int n = _wtoi(str.c_str());
					Sleep( 50 );
				}
			}
			else
			{
			    wstring full = soundPath + file;

				DWORD attrib = GetFileAttributes( full.c_str() );
				if ( attrib != INVALID_FILE_ATTRIBUTES )
				{
					sndPlaySound(full.c_str(), SND_SYNC);
				}
			}
		}
		else
		{
			Sleep(200);
		}
	}

	return 0;
}

const wchar_t *SoundPlay::getLevelSoundLocation(int level)
{
    switch (level)
    {
        case 0x1:
            return  L"c1f.wav";
        case 0x2:
            return  L"c2f.wav";
        case 0x3:
            return  L"c3f.wav";
        case 0x4:
            return  L"c1l.wav";
        case 0x5:
            return  L"c2l.wav";
        case 0x6:
            return  L"b1f.wav";
        case 0x7:
            return  L"b2f.wav";
        case 0x8:
            return  L"b1l.wav";
        case 0x9:
            return  L"b1r.wav";
        case 0xa:
            return  L"h1f.wav";
        case 0xb:
            return  L"x1f.wav";
        case 0xc:
            return  L"c3l.wav";
        case 0xd:
            return  L"c4f.wav";
        case 0xe:
            return  L"c4l.wav";
        case 0xf:
            return  L"x2f.wav";
        case 0x10:
            return  L"x3f.wav";
        case 0x11:
            return  L"x1l.wav";
        case 0x12:
            return  L"x2l.wav";
        case 0x13:
            return  L"b3f.wav";
        case 0x14:
            return  L"b4f.wav";
        case 0x15:
            return  L"b2l.wav";
        case 0x16:
            return  L"b3l.wav";
        case 0x17:
            return  L"b2r.wav";
        case 0x18:
            return  L"b3r.wav";
        case 0x19:
            return  L"h2f.wav";
        case 0x1a:
            return  L"x2v.wav";
        case 0x1b:
            return  L"c3v.wav";
        case 0x1c:
            return  L"b2v.wav";
        case 0x1d:
            return  L"b3v.wav";
        case 0x1e:
            return  L"s1.wav";
        case 0x1f:
            return  L"b4l.wav";
        case 0x20:
            return  L"x3l.wav";
        case 0x21:
            return  L"x4l.wav";
        case 0x22:
            return  L"x4f.wav";
        case 0x23:
            return  L"s2.wav";
        case 0x24:
            return  L"cx1k.wav";
        case 0x25:
            return  L"cx2k.wav";
        case 0x26:
            return  L"b1k.wav";
        case 0x27:
            return  L"b2k.wav";
        case 0x28:
            return  L"b3k.wav";
        case 0x29:
            return  L"gy1.wav";
        case 0x2a:
            return  L"gy2.wav";
		case 0x2b:
			return  L"jwy.wav";

        default:
            return  L"";
    }
}

void SoundPlay::snd_integer(int num, int digit)
{
	if ( num == '.' )
	{
		playFile( L"��������/dian.wav" );
		return;
	}

    wchar_t snd[100];
	if ( num == '0' )
		digit = 1;
    switch( digit )
    {
        case 4: /* ǧ */
            swprintf( snd, L"��������/%c000.wav", num );
            break;
        case 3: /* �� */
            swprintf( snd, L"��������/%c00.wav", num );
            break;
        case 2: /* ʮ */
            swprintf( snd, L"��������/%c0.wav", num );
            break;
        case 1: /* �� */
            swprintf( snd, L"��������/%c.wav", num );
            break;
        case 0: /* �� */
            swprintf( snd, L"��������/%c.wav", num );
            break;
    }
    playFile(snd);
}

void SoundPlay::playNumberStr( const char* weight, int digit )
{
	int len = len = strlen(weight);
	int i, temp;
    for (i = 0, temp = 0; i < len; i++)
    {
        if (digit >= 0)
        {
            temp = digit;
            digit--;
        }
        else
        {
            temp = 1;
        }
        if (digit > 0 && (weight[i] == '0') && (weight[i+1] == '0') )
            continue;
        if ((i != 0) && (weight[i] == '0') && (weight[i+1] == '.'))
            continue;
        if ((weight[i] == '0') && (weight[i+1] == '\0'))
            break;
        snd_integer(weight[i], temp);
    }
}

void SoundPlay::snd_playweight(const char *weight)
{
    int digit = 0, i, len;

    len = strlen(weight);

    for (i = 0; i < len; i++)
    {
        if (weight[i] == '.')
        {
            digit = i;
            break;
        }
    }

	if ( digit > 4 )
	{
		int wanwei = digit - 4;
		std::string wan( weight, wanwei );
		playNumberStr( wan.c_str(), wanwei );
		playFile( L"��������/wan.wav" );

		digit = 4;
		weight += wanwei;
	}
	playNumberStr( weight, digit );

    //playFile( L"��������/gongj.wav");
}

void SoundPlay::playLevel(int lev)
{
	if (lev != 0)
	{
		wchar_t buf[100];
		swprintf( buf, L"�ȼ�����/DJ_%s", getLevelSoundLocation(lev) );

		playFile( buf );
	}
}

void SoundPlay::weightinfo( int level, float weight )
{
	if (level != 0)
	{
		playLevel(level);
	}

	playFile( L"*Sleep 500");
	char buf[100];
	sprintf( buf, "%.2f", weight );
	snd_playweight(buf);
}

void SoundPlay::welcome()
{
    playFile( L"�չ�����/��ӭ.wav");
}

void SoundPlay::carToLocation( const std::wstring& car, const std::wstring& location )
{
	playFile( L"�ִ�����/��.wav");
	playString( car );
	playFile( L"�ִ�����/��.wav" );
	playString( location );
	playFile( L"�ִ�����/ж��λ���н���.wav" );
}

void SoundPlay::playString( const std::wstring& str )
{
	wchar_t snd[ 100 ];
	const wchar_t* p = str.c_str();
	while ( *p )
	{
		unsigned int ch = *p++;
		if (isdigit( ch ) )
		{
			swprintf( snd, L"��������/%c.wav", ch );
		}
		else
		{
			swprintf( snd, L"�ִ�����/%c.wav", ch );
		}
		playFile(snd);
	}
}

void SoundPlay::setSoundPath( const std::wstring& path ) 
{
	soundPath = path;

	if ( path[path.length() - 1] != '\\' )
	{
		soundPath.append( L"\\" );
	}
} 
