#include "StdAfx.h"
#include "DevConfig.h"
#include <windows.h>
#include <commctrl.h>
#include <winreg.h>

#include <ntddndis.h>
#include <winioctl.h>
#include <winsock2.h>

#include "DtCov.h"

DevConfig::DevConfig(void)
{
}

DevConfig::~DevConfig(void)
{
}

void DevConfig::RebindNdis( LPCWSTR strAdapterName )
{

}

CString DevConfig::GetLocalIp( LPCWSTR strAdapterName )
{
	CString ret;
	char HostName[100];

	gethostname(HostName, sizeof(HostName));// 获得本机主机名.

	hostent* hn;
	hn = gethostbyname(HostName);//根据本机主机名得到本机ip

	if (hn != NULL)
	{
        ret = inet_ntoa(*(struct in_addr *)hn->h_addr_list[0]);
	}

	return ret;
}

BOOL DevConfig::SetIpConfig(LPCWSTR strAdapterName, LPCWSTR m_StrIPAddress, LPCWSTR m_StrIPMask, LPCWSTR m_StrGate, DWORD useDHCP)
{
	long lResult = 0;

	wchar_t strSubKeyName [128];
	swprintf( strSubKeyName , L"Comm\\%s\\Parms\\TcpIp", strAdapterName );

	LPCTSTR strIP = _T("IpAddress");
	LPCTSTR strMask = _T("SubnetMask");
	LPCTSTR strGate = _T("DefaultGateway");
	LPCTSTR strDHCP = _T("EnableDHCP");

	// Change IP Settings
	HKEY hKey;
	DWORD dwStrLen = 0;
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, strSubKeyName, 0, 0, &hKey) == ERROR_SUCCESS)
	{
		dwStrLen = (DWORD)wcslen(m_StrIPAddress) * sizeof(WCHAR);
		lResult = RegSetValueEx(hKey, strIP, 0, REG_SZ, (LPBYTE)m_StrIPAddress, dwStrLen);
		if(lResult != ERROR_SUCCESS)
			return FALSE;

		dwStrLen = (DWORD)wcslen(m_StrIPMask) * sizeof(WCHAR);
		lResult = RegSetValueEx(hKey, strMask, 0, REG_SZ, (LPBYTE)m_StrIPMask, dwStrLen);
		if(lResult != ERROR_SUCCESS)
			return FALSE;

		dwStrLen = (DWORD)wcslen(m_StrGate) * sizeof(WCHAR);
		lResult = RegSetValueEx(hKey, strGate, 0, REG_SZ, (LPBYTE)m_StrGate, dwStrLen);
		if(lResult != ERROR_SUCCESS)
			return FALSE;

		dwStrLen = sizeof(useDHCP);
		lResult = RegSetValueEx(hKey, strDHCP, 0, REG_DWORD, (LPBYTE)&useDHCP, dwStrLen); 
		if(lResult != ERROR_SUCCESS)
			return FALSE;

		RegFlushKey(hKey);
		RegCloseKey(hKey);

		RebindNdis( strAdapterName );

		return TRUE;
	}

	return FALSE;
}

BOOL DevConfig::GetIpConfig(LPCWSTR strAdapterName, LPWSTR m_StrIPAddress, LPWSTR m_StrIPMask, LPWSTR m_StrGate, DWORD& useDHCP )
{
	BOOL ret = TRUE;

	long lResult = 0;

	wchar_t strSubKeyName [128];
	swprintf( strSubKeyName , L"Comm\\%s\\Parms\\TcpIp", strAdapterName );

	LPCTSTR strIP = _T("IpAddress");
	LPCTSTR strDhcpIP = _T("DhcpIPAddress");
	LPCTSTR strMask = _T("SubnetMask");
	LPCTSTR strGate = _T("DefaultGateway");
	LPCTSTR strDHCP = _T("EnableDHCP");

	// Change IP Settings
	HKEY hKey;
	DWORD dwStrLen = 0;
	if(RegOpenKeyEx(HKEY_LOCAL_MACHINE, strSubKeyName, 0, 0, &hKey) == ERROR_SUCCESS)
	{
		DWORD type;
		dwStrLen = 32;
		lResult = RegQueryValueEx(hKey, strMask, 0, &type, (LPBYTE)m_StrIPMask, &dwStrLen);
		if(lResult != ERROR_SUCCESS)
			ret = FALSE;

		dwStrLen = 32;
		lResult = RegQueryValueEx(hKey, strGate, 0, &type, (LPBYTE)m_StrGate, &dwStrLen);
		if(lResult != ERROR_SUCCESS)
			ret = FALSE;

		dwStrLen = sizeof(useDHCP);
		lResult = RegQueryValueEx(hKey, strDHCP, 0, &type, (LPBYTE)&useDHCP, &dwStrLen); 
		if(lResult != ERROR_SUCCESS)
			ret = FALSE;

		dwStrLen = 32;
		if (useDHCP)
		{
			lResult = RegQueryValueEx(hKey, strDhcpIP, 0, &type, (LPBYTE)m_StrIPAddress, &dwStrLen);
		}
		else
		{
			lResult = RegQueryValueEx(hKey, strIP, 0, &type, (LPBYTE)m_StrIPAddress, &dwStrLen);
		}
		if(lResult != ERROR_SUCCESS)
			ret = FALSE;

		RegCloseKey(hKey);

	}

	return ret;
}

BOOL DevConfig::CreateReg(WCHAR *key, LPCWSTR val)
{
	HKEY hMKey = NULL;
	HKEY hSKey = NULL;
	DWORD dwStrLen;

	if (RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software"), 0, KEY_WRITE|KEY_READ, &hMKey) == ERROR_SUCCESS)
	{
		DWORD dw;
        dwStrLen = (DWORD)wcslen(val) * sizeof(WCHAR);

		if (RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\DevConfig"), 0, KEY_WRITE|KEY_READ, &hSKey) == ERROR_SUCCESS)
		{
	        RegSetValueEx(hSKey, key, 0, REG_SZ, (LPBYTE)val, dwStrLen);
		}
		//创建并打开HKEY_CURRENT_USER\"Software"\"DevConfig"
		else if (RegCreateKeyEx(hMKey, _T("DevConfig"), 0, REG_NONE,
			REG_OPTION_NON_VOLATILE, KEY_WRITE|KEY_READ, NULL,
			&hSKey, &dw) == ERROR_SUCCESS)
		{
			RegSetValueEx(hSKey, key, 0, REG_SZ, (LPBYTE)val, dwStrLen);
		}
	}

	//关闭打开的键值。
	if (hMKey != NULL)
	{
		RegCloseKey(hMKey);
	}   

	if (hSKey != NULL)
	{
		RegCloseKey(hSKey);
	}

	return TRUE;
}

BOOL DevConfig::GetReg(WCHAR *key, WCHAR *buf, DWORD dwStrLen)
{
	BOOL ret = TRUE;
	long lResult = 0;
	WCHAR *strSubKeyName = _T("Software\\DevConfig");
	HKEY hKey;

	if(RegOpenKeyEx(HKEY_CURRENT_USER, strSubKeyName, 0, KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
	{
		DWORD type;

		lResult = RegQueryValueEx(hKey, key, 0, &type, (LPBYTE)buf, &dwStrLen);
		if(lResult != ERROR_SUCCESS)
			ret = FALSE;

		RegCloseKey(hKey);
	}
	else
	{
	    ret = FALSE;
	}

	return ret;
}

BOOL DevConfig::GetServerIp(LPWSTR ip, LPWSTR port)
{
	BOOL ret = TRUE;

    if (!GetReg(_T("IpAddr"), ip, 32))
	{
        CreateReg(_T("IpAddr"), _T("192.168.1.128"));
		ret = FALSE;
	}

	if (!GetReg(_T("Port"), port, 32))
	{
		CreateReg(_T("Port"), _T("5555"));
		ret = FALSE;
	}

	return ret;
}

BOOL DevConfig::SetServerIp(LPCWSTR ip, LPCWSTR port)
{
	CreateReg(_T("IpAddr"), ip);
	CreateReg(_T("Port"), port);

    return TRUE;
}

BOOL DevConfig::GetServerIp(CString &ip, int &port)
{
    WCHAR wport[32];
    WCHAR wip[32];

	GetServerIp(wip, wport);
    port = _ttoi(wport);
    ip = wip;

	return TRUE;
}

BOOL DevConfig::IsIP(CString &s)
{
    BOOL ret = FALSE;
    DtCov cov;
	string ip;
	int pos, start = 0;
	string sub;
	int i;

	ip = cov.wtoc((LPCWSTR)s);

	pos = (int)ip.length();
	if (pos < 7 || pos > 15)
		goto EXIT;

	for (i = 0; i < pos; i ++)
	{
        if (((ip.at(i) > '9') || (ip.at(i) < '0')) && (ip.at(i) != '.'))
			goto EXIT;
	}

	for (i = 0; i < 4; i ++)
	{
        pos = (int)ip.find(".", start);
		sub = ip.substr(start, pos - start);

		start = pos + 1;
		if (start == 0 && i != 3)
			goto EXIT;

		int val;
        if (sub == "")
			goto EXIT;

		val = atoi(sub.c_str());
		if (val > 255)
			goto EXIT;
	}

	ret = TRUE;

EXIT:

	return ret;
}

BOOL DevConfig::GetDevSn(string &sn)
{
	WCHAR buf[32] = {_T("BM34260025")};
	DWORD len = 32;
    char str[32];

	if (!GetReg(_T("DevSn"), buf, len))
	{
        CreateReg(_T("DevSn"), buf);
	}

	len = (DWORD)wcslen(buf);
	for (DWORD i = 0; i <= len; i ++)
	{
		str[i] = (char)buf[i];
	}

	sn = str;

	return TRUE;
}

BOOL DevConfig::GetDevSn(CString &sn)
{
	WCHAR buf[32] = {_T("BM12345678")};
	DWORD len = 32;

	if (!GetReg(_T("DevSn"), buf, len))
	{
		CreateReg(_T("DevSn"), buf);
	}

	sn = buf;

	return TRUE;
}

BOOL DevConfig::GetComKeyboard(WCHAR *buf, int size)
{
	BOOL ret = TRUE;

	if (!GetReg(_T("ComKeyboard"), buf, size))
	{
		wcscpy(buf, _T("COM3"));
        ret = FALSE;
	}

    return ret;
}

BOOL DevConfig::GetComScale(WCHAR *buf, int size)
{
	BOOL ret = TRUE;

	if (!GetReg(_T("ComScale"), buf, size))
	{
		wcscpy(buf, _T("COM4"));
		ret = FALSE;
	}

	return ret;
}

void DevConfig::SetComKeyboard(LPCWSTR buf)
{
    CreateReg(_T("ComKeyboard"), buf);
}

void DevConfig::SetComScale(LPCWSTR buf)
{
    CreateReg(_T("ComScale"), buf);
}
