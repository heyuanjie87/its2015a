#include "StdAfx.h"
#include "Keyboard.h"
#include "DevConfig.h"

Keyboard::Keyboard(void)
{
	Connected = false;
}

Keyboard::~Keyboard(void)
{
	Close();
}

bool Keyboard::Open()
{
	return Start();
}

bool Keyboard::Close()
{
    isRunning = false;

	return true;
}

bool Keyboard::Alive()
{
	return Connected;
}

unsigned char Keyboard::Crc8(unsigned char *buf, short len)
{
	unsigned char x1=0;
	unsigned char x2=0;
	unsigned char crc=0;

	if(buf == NULL)
		return 0;

	for(short i = 0; i < len; i ++)
	{
		x2 = buf[i];
		x1 ^= x2;
	}

	crc = x1;

	return crc;
}

int Keyboard::GetKey(bool SendReq)
{
	int key = -1;
	unsigned char req[] = {0x55, 0xAA, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF};
	unsigned char rsp[8];
	int size;

	Dev.lock();

	if (SendReq)
	{
		req[7] = Crc8(req, 7);
		Dev.write(req, sizeof(req));
	}
    Dev.setretry(3);
	size = Dev.read(rsp, 8);
	if (size > 0)
	{
		if ((rsp[2] == 1) && rsp[7] == Crc8(rsp, 7))
		{
            key = rsp[6];
		}
		else
		{
            Dev.clearbuf();
			printf("Clr %d\n", size);
			for (int i = 0; i < size; i ++)
			{
                printf("[%0X]", rsp[i]);
			}
			printf("\n");
		}
	}

	Dev.unlock();

	return key;
}

int Keyboard::Run(void)
{
    BYTE kcode;
    int key = 0;
	DevConfig cfg;
	WCHAR port[6];
    int n = 0;
	int errcnt;
	bool ret;
	bool SendReq = false;
	time_t tm;

    keymapInit();
	isRunning = true;

	cfg.GetComKeyboard(port, sizeof(port));

INIT:
	if (!isRunning)
		goto EXIT;
    
	Connected = false;
	tm = time(NULL);
	errcnt = 0;
	ret = Dev.open(port, 115200);
    n ++;

    if (!ret)
	{
		if (n >= 10)
		{
			n = 1;
		}
		
		swprintf(port, L"COM%d", n);

		Dev.close();
		Sleep(100);
        goto INIT;
	}

	if (GetKey(true) != -1)
	{
		Connected = true;
        cfg.SetComKeyboard(port);
	}
	else
	{
		Dev.close();
		swprintf(port, L"COM%d", n);
		Sleep(100);
        goto INIT;
	}

	while (isRunning)
	{
        Sleep(25);

        key = GetKey(SendReq);

		if (key >= 0)
		{
            SendReq = false;
            errcnt = 0;
			tm = time(NULL);
			Connected = true;
		}

		if (key > 0)
		{
			kcode = keymap[key];

 			keybd_event(kcode, 0, 0, 0);
			keybd_event(kcode, 0, KEYEVENTF_KEYUP, 0);
		}

		if (SendReq && key == -1)
		{
            errcnt++;
            printf("KbdErr\n");
			Sleep(100);
			if (errcnt > 3)
			{
			    //重新枚举设备
                Dev.close();
                swprintf(port, L"COM%d", n);
			    goto INIT;
			}
		}

		if ((time(NULL) - Dev.rsptime()) > 2)
		{
			//如果2秒后没有按键则主动查询
		    SendReq = true;
		}
	}

EXIT:
	Dev.close();

	return 0;
}

void Keyboard::keymapInit(void)
{
	BYTE keys1[][5] = 
	{
		{ 0x50, 0x40, 0x30, 0x20, 0x10 },
		{ 0x51, 0x41, 0x31, 0x21, 0x11 },
		{ 0x52, 0x42, 0x32, 0x22, 0x12 },
		{ 0x53, 0x43, 0x33, 0x23, 0x13 },
		{ 0x54, 0x44, 0x34, 0x24, 0x14 },
		{ 0x55, 0x45, 0x35, 0x25, 0x15 },
	};

	BYTE keys2[][5] = 
	{
		{ KEY_11, KEY_12, KEY_13, KEY_14, KEY_15 },
		{ KEY_21, KEY_22, KEY_23, KEY_24, KEY_25 },
		{ KEY_31, KEY_32, KEY_33, KEY_34, KEY_35 },
		{ KEY_41, KEY_42, KEY_43, KEY_44, KEY_45 },
		{ KEY_51, KEY_52, KEY_53, KEY_54, KEY_55 },
		{ KEY_61, KEY_62, KEY_63, KEY_64, KEY_65 },
	};

	memset( keymap, 0, sizeof( keymap ) );

	for ( int i = 0; i < 6; ++i )
	{
		for ( int j = 0; j < 5; ++j )
		{
			int key = keys1[i][j];
			BYTE data = keys2[i][j];
			keymap[key] = data;
		}
	}
}
