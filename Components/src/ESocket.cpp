// ESocket.cpp : 实现文件
//

#include "stdafx.h"
#include "ESocket.h"


// ESocket

ESocket::ESocket()
{
	m_TxTimeout = 2000;
	m_RxTimeout = 4000;
}

ESocket::~ESocket()
{
}


// ESocket 成员函数
BOOL ESocket::OnMessagePending()
{
	MSG msg;

	if(::PeekMessage(&msg, NULL, WM_TIMER, WM_TIMER, PM_NOREMOVE))
	{
		if (msg.wParam == (UINT) m_nTimerID)
		{
			KillTimer(NULL, m_nTimerID);
			::PeekMessage(&msg, NULL, WM_TIMER, WM_TIMER, PM_REMOVE);
			CancelBlockingCall();

			return FALSE;
		};
	};

	return CSocket::OnMessagePending();
}

bool ESocket::Timeout(UINT rxtm, UINT txtm)
{
    m_TxTimeout = txtm;
	m_RxTimeout = rxtm;

	return true;
}

BOOL ESocket::Connect(LPCTSTR lpszHostAddress, UINT nHostPort)
{
    BOOL ret;

    m_nTimerID = SetTimer(NULL, 0, 2000, NULL);
	ret = CSocket::Connect(lpszHostAddress, nHostPort);
	KillTimer(NULL, m_nTimerID);

	return ret;
}

int ESocket::Receive(void* lpBuf, int nBufLen, int nFlags)
{
	int ret;

	m_nTimerID = SetTimer(NULL, 0, m_RxTimeout, NULL);
	ret = CSocket::Receive(lpBuf, nBufLen, nFlags);
    KillTimer(NULL, m_nTimerID);

	return ret;
}

int ESocket::Send(const void* lpBuf, int nBufLen, int nFlags)
{
	int ret;

    m_nTimerID = SetTimer(NULL, 0, m_TxTimeout, NULL);
	ret =  CSocket::Send(lpBuf, nBufLen, nFlags);
	KillTimer(NULL, m_nTimerID);

	return ret;
}
