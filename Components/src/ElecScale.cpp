#include "StdAfx.h"
#include "ElecScale.h"
#include "usermsg.h"
#include "DevConfig.h"

#define WEIGHTLENGTH    19
#define SCALEIDLENGTH   11

ElecScale::ElecScale(void)
: msgReceiver(NULL)
, NWeight(0.0)
, TWeight(0.0)
, StatOk(false)
{
	Connected = false;
	isReadingScaleId = false;
    needCheckScaleId = false;
	ScaleId[0] = 0;
	ZeroDone = 0;
}

ElecScale::~ElecScale(void)
{
}

bool ElecScale::IsZeroDone()
{
	return (ZeroDone == 1);
}

bool ElecScale::IsConnected()
{
    return Connected;
}

int ElecScale::Run()
{
	CTime tm;
	wchar_t port[8];
	DevConfig cfg;
	int n = 1;
	int errcnt;
	bool ret;

	isRunning = true;

    cfg.GetComScale(port, sizeof(port));

INIT:
	if (!isRunning)
		goto EXIT;

	errcnt = 0;
	ret = ComDev.open(port, CBR_9600);
    //printf("Init %s\n", port);
    n ++;
	if (!ret)
	{
		if (n == 15)
		{
			n = 2;
		}

		swprintf(port, L"COM%d", n);
        ComDev.close();
		Sleep(200);
		goto INIT;
	}

	if (ReadId())
	{
		cfg.SetComScale(port);
	}

	while (isRunning)
	{
		weighting();

		if (ZeroStart)
		{
			if (Connected)
			{
                tm = tm.GetCurrentTime();

				if (NWeight < -ZeroDevi || NWeight > ZeroDevi)
				{
                    ZStartTime = tm.GetTime();
				}

				if ((tm.GetTime() - ZStartTime) >= ZeroTime)
				{
                    ZeroDone = 1;
                    ZeroStart = 0;

					if (MsgEn_Zero && msgReceiver)
					{
                        PostThreadMessage(msgReceiver, WM_ELECSCALE_ZERO, 0, 0);
					}
				}
			}
		}

		if (Connected)
		{
            errcnt = 0;
		}
		else
		{
			errcnt ++;

			if (errcnt > 3)
			{
				ComDev.close();
				swprintf(port, L"COM%d", n);
				Sleep(50);
				TRACE0("Err\n");
				goto INIT;
			}	
		}

		Sleep(freq);
	}

EXIT:
    ComDev.close();

	return 0;
}

bool ElecScale::weighting()
{
	bool ret;

	if (isReadingScaleId)
	{
        ret = ReadId();

		if (ret)
			isReadingScaleId = false;
	}
	else
	{
		ret = ReadWeight();
	}

	return ret;
}

char* ElecScale::getScaleId()
{
	return ScaleId;
}

void ElecScale::SetIdCheck(bool en, string id)
{
    needCheckScaleId = en;
	m_Id = id;
}

void ElecScale::GetIdCheck(string &stat, string &id)
{
    if (!needCheckScaleId)
	{
		stat = "0";
	}
	else
	{
		if (m_Id == ScaleId)
		{
            stat = "0";
		}
		else
		{
			stat = "1";
		}
	}

    id = ScaleId;
}

void ElecScale::startWeight(int frequence)
{
	if (isRunning)
		return;

    freq = frequence;

	Start();
}

void ElecScale::endWeight()
{
	isRunning = false;
}

/*
  读仪表ID
  发送命令：STX，R，D，X，n, BCC，CR
  仪表应答：STX, n, A, D1, D2, D3, D4, D5, D6, BCC, CR
*/
bool ElecScale::ReadId()
{
	static unsigned char idcmd[] = {0x02, 0x52, 0x44, 0x58, 0x01, 0xEF, 0x0D};
    bool ret;
	int size;

	write(idcmd, sizeof(idcmd));
    size = readExact(weight_data, SCALEIDLENGTH);

    ret = parseScaleId(weight_data, size);

	return ret;
}

/*
  读仪表重量
  发送命令：STX，R，D，S，n, BCC，CR
  仪表应答：STX, n, N, X1, X2, X3, X4, X5, X6, Y1,Y2,Y3,Y4,Y5,Y6, SA, BCC, CR
*/
bool ElecScale::ReadWeight()
{
	static unsigned char wcmd[] = {0x02, 0x52, 0x44, 0x53, 0x01, 0xEA, 0x0D};
	bool ret = false;
    int size;

	write(wcmd, sizeof(wcmd));
    size = readExact(weight_data, WEIGHTLENGTH);

    ret = parseWeightValue(weight_data, size);

	return ret;
}

bool ElecScale::parseWeightValue(unsigned char *buf, int size)
{
    char w[16];
    int i, j;
    char status = 1;
	float weight = 0;
	float tare = 0;
	bool ret = false;

	if (size != WEIGHTLENGTH)
		return false;

    if (buf[0] != 0x02 || buf[1] != 0x01 || buf[2] != 'N' || buf[18] != 0x0D)
        goto err;

    if ((buf[16] & 0x8) == 0x8) /* 称重溢出 */
        goto err;

    if ((buf[16] & 0x2) != 0x2) /* 称重不稳定 */
        goto err;

    /* 当前重量 */
    for (i = 0, j = 0; i < 7; i++)
    {
        if (buf[9-i] == 0x20)
            continue;
        w[j++] = buf[9-i];
    }
    w[j++] = '\0';
	weight = (float)atof( w );
   
	j = 0;
    /* 皮重 */
    for (i = 0; i < 6; i++)
    {
        if (buf[15-i] == 0x20)
            continue;
        w[j++] = buf[15-i];
    }
    w[j++] = '\0';
	tare = (float)atof( w );

    status = 0;

err:
    processMessage(status, weight, tare);

	return ret;
}

bool ElecScale::parseScaleId(unsigned char *buf, int size)
{
    unsigned char w[16];
    int i, j;
	bool ret = true;

    char status = 1;

    w[0] = 0;

    if (size != SCALEIDLENGTH)
		return false;

    if (buf[0] != 0x02 || buf[1] != 0x01 || buf[2] != 'A' || buf[10] != 0x0D)
    {
		ret = false;
    }
	else
	{
		for (i = 0, j = 0; i < 6; i++)
		{
			w[j++] = buf[8-i];
		}
		w[j] = 0;

		strcpy(ScaleId, (char*)w );
	}

	return ret;
}

int ElecScale::readExact(unsigned char* buf, unsigned int len)
{
	int size;

	size = ComDev.read(buf, len);

	if (size <= 0)
	{
        Connected = false;
	}
	else
	{
        Connected = true;
	}

	return size;
}

int ElecScale::write( unsigned char* buf, unsigned int len )
{
	return ComDev.write( buf, len );
}

void ElecScale::processMessage( int status, float weight, float tare )
{
	if ( msgReceiver )
	{
		if ( status )
			weight = 0;
	}
    TRACE1("Weight %f\n", weight);
	if (!Lock)
	{
		Lock = true;

	    NWeight = weight;
	    TWeight = tare;
	    StatOk = (status == 0);

		Lock = false;
	}
}

bool ElecScale::GetWeight(float &nw, float &tw)
{
    Lock = true;
    nw = NWeight;
	tw = TWeight;
	Lock = false;

	return StatOk;
}

void ElecScale::EnableMsg(EsMsgEn en)
{
    if (en == ES_MSG_ZERO)
	{
        MsgEn_Zero = 1;
	}
}

void ElecScale::StartZeroCheck(int sec, float devi)
{
	CTime tm;

    ZeroTime = sec;
    ZeroDevi = devi;

	isReadingScaleId = true;

	tm = tm.GetCurrentTime();
	ZStartTime = tm.GetTime();
    ZeroDone = 0;
	ZeroStart = 1;
}

