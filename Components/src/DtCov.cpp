#include "StdAfx.h"
#include "DtCov.h"

DtCov::DtCov(void)
{
}

DtCov::~DtCov(void)
{
}

string DtCov::ftos(float &f)
{
    char buf[20];

	sprintf(buf, "%.2f", f);

	return string(buf);
}

float DtCov::stof(string &s)
{
    return (float)atof(s.c_str());
}

int DtCov::stoi(string &s)
{
    return (int)atoi(s.c_str());
}

string DtCov::itos(int i)
{
	char buf[20];

	sprintf(buf, "%d", i);

	return string(buf);
}

string DtCov::wtoc(LPCWSTR src)
{
	int nLen = WideCharToMultiByte(CP_ACP, 0, src, -1, NULL, 0, NULL, NULL);

	if (nLen<= 0) 
		return string("");

	char* pszDst = new char[nLen];

	if (NULL == pszDst) 
		return string("");

	WideCharToMultiByte(CP_ACP, 0, src, -1, pszDst, nLen, NULL, NULL);
	pszDst[nLen -1] = 0;

	string strTemp(pszDst);
	delete [] pszDst;

	return strTemp;
}

string DtCov::ctoutf8(string &src)
{
	string dest;
	int len;
	wchar_t* woutbuf;
    int n;

	len = (int)src.length() + 1;
    woutbuf = new wchar_t[len];

	char* outbuf = new char[len*3];

	n = MultiByteToWideChar(936, 0, src.c_str(), len, woutbuf, len);
	n = WideCharToMultiByte( CP_UTF8, 0, woutbuf, n, outbuf, len*3, NULL, NULL );
	outbuf[n] = 0;

	dest = outbuf;

	delete outbuf;
	delete woutbuf;

	return dest;
}
