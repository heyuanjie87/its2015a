#include "StdAfx.h"
#include "RfCardReader.h"

RfCardReader::RfCardReader(void)
{
}

RfCardReader::~RfCardReader(void)
{
	Close();
}

bool RfCardReader::Open(void)
{
    return m_Rfid.Open(_T("COM4"), 115200);
}

bool RfCardReader::Close(void)
{
    return m_Rfid.Close();
}

bool RfCardReader::Alive(void)
{
	bool ret;

    m_Rfid.Lock();

    ret = m_Rfid.Alive();

	m_Rfid.Unlock();

	return ret;
}

bool RfCardReader::WorkerCardRead(WorkerInfo &wi)
{
	unsigned char pwd[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
	bool ret = false;
	unsigned char buf[20];

	memset(buf, 0, sizeof(buf));

	m_Rfid.Lock();

    //���
	if (!m_Rfid.Authen(1, TYPE_A, pwd, 6))
	{
		goto EXIT;
	}
	if (!m_Rfid.BlockRead(1, buf, 16))
	{
		goto EXIT;
	}

	wi.Number = (const char*)buf;
    if (wi.Number.empty())
        goto EXIT;

	//����
	if (!m_Rfid.Authen(2, TYPE_A, pwd, 6))
	{
		goto EXIT;
	}
	if (!m_Rfid.BlockRead(2, buf, 16))
	{
		goto EXIT;
	}
    
	wi.Name = (const char*)buf;
	if (wi.Name.empty())
		goto EXIT;

	ret = true;

EXIT:
	m_Rfid.Unlock();

	return ret;
}

bool RfCardReader::CreditCardRead(string &cardid)
{
	unsigned char buf[128];
	unsigned char cos1[] = {0x00, 0xA4, 0x04, 0x00, 0x08, 0xA0, 0x00, 0x00, 0x03, 0x33, 0x01, 0x01, 0x01};
	unsigned char cos2[] = {0x00, 0xB2, 0x01, 0x14, 0x00};
	short size;
	char str[24];
	bool ret = false;

	m_Rfid.Lock();

	if (!m_Rfid.CpuCardMode())
	{
		goto EXIT;
	}

	m_Rfid.CosSend(cos1, sizeof(cos1));
	m_Rfid.AckRecv(buf, 19);

	m_Rfid.CosSend(cos2, sizeof(cos2));    
	size = m_Rfid.AckRecv(buf, sizeof(buf));

	if (size != 0)
	{
        int i;

		for (i = 0; i < size; i ++)
		{
			if (buf[i] == 0x5A)
				break;
		}

		if (i == size)
            goto EXIT;
		
		i ++;

		size = BCD2Str(str, &buf[i+1], buf[i]);
        if ((size+1)/2 != buf[i])
			goto EXIT;

		cardid = str;

        ret = true;
	}

EXIT:
	m_Rfid.Unlock();

	return ret;
}

int RfCardReader::CardScan(RfidInfo *rfidinfo)
{
	int ret;

    m_Rfid.Lock();

    ret = m_Rfid.CardScan(rfidinfo);

	m_Rfid.Unlock();

	return ret;
}

void RfCardReader::Lock()
{
    m_Rfid.Lock();
}

void RfCardReader::Unlock()
{
    m_Rfid.Unlock();
}

int RfCardReader::BCD2Str(char* dst, unsigned char *src, short n)
{
	unsigned char h, l;
	int i, j;
	int len = 0;

	for (i = 0, j = 0; i < n; i ++, j += 2)
	{
		h = src[i] >> 4;
		l = src[i] &0xF;

		if (h > 9)
		{
            dst[j] = 0;
			break;
		}
		else
		{
			len ++;
            dst[j] = h + '0';
		}

		if (l > 9)
		{
			dst[j+1] = 0;
			break;
		}
		else
		{
			len ++;
			dst[j+1] = l + '0';
		}
	}

	dst[len] = 0;

	return len;
}
