#include "StdAfx.h"
#include "SerialPort.h"

SerialPort::SerialPort(void):
hFile(INVALID_HANDLE_VALUE),
retry(3)
{
}

SerialPort::~SerialPort(void)
{
	close();
}

bool SerialPort::open(const wchar_t *portName, int baudRate)
{
    hFile = CreateFile(portName,
		               GENERIC_READ | GENERIC_WRITE,
					   0,
					   NULL,
					   OPEN_EXISTING,
					   FILE_ATTRIBUTE_NORMAL,
					   NULL);
		               
    if (hFile == INVALID_HANDLE_VALUE)
	{
		return false;
	}

	SetupComm(hFile, 128, 128);

	COMMTIMEOUTS TimeOuts;

	TimeOuts.ReadIntervalTimeout= 5;
	TimeOuts.ReadTotalTimeoutMultiplier= 4;
	TimeOuts.ReadTotalTimeoutConstant= 20;

	TimeOuts.WriteTotalTimeoutMultiplier=10;
	TimeOuts.WriteTotalTimeoutConstant=10;

	SetCommTimeouts(hFile,&TimeOuts);
	SetCommMask(hFile, EV_RXCHAR);

	DCB dcb;

	dcb.DCBlength = sizeof(dcb);
	if (!GetCommState(hFile, &dcb))
	{
		return false;
	}

	dcb.BaudRate = baudRate;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;

	if (!SetCommState(hFile, &dcb))
	{
		return false;
	}

    PurgeComm(hFile, PURGE_TXCLEAR | PURGE_RXCLEAR);
	
	return true;
}

bool SerialPort::close()
{
    if (hFile != INVALID_HANDLE_VALUE)
	{
		CloseHandle(hFile);
		hFile = INVALID_HANDLE_VALUE;
	}

	return true;
}

int SerialPort::write(unsigned char *buf, int size)
{
	DWORD Errors;
	COMSTAT Stat;
    DWORD dwBytes = 0;

	if (hFile == INVALID_HANDLE_VALUE)
	    return 0;

    ClearCommError(hFile, &Errors, &Stat);

    WriteFile(hFile, buf, size, &dwBytes, NULL);

	return dwBytes;
}

void SerialPort::clearbuf()
{
    PurgeComm(hFile, PURGE_TXCLEAR | PURGE_RXCLEAR);	 /* �����/�������� */
}

void SerialPort::setretry(int cnt)
{
    retry = cnt;
}

int SerialPort::read(unsigned char *buf, int size)
{
	DWORD dwBytes;
    int num = 0;
	int len = 0;

    if (hFile == INVALID_HANDLE_VALUE)
		return 0;

	while (size)
	{
        ReadFile(hFile, buf, size, &dwBytes, NULL);

		if (dwBytes > 0)
		{
			buf += dwBytes;
			size -= dwBytes;
			len += dwBytes;
			num = 0;
		}
		else
		{
			num++;

			if (num >= retry)
			{
				break;
			}
		}
	}

	return len;
}