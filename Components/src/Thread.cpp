#include "StdAfx.h"
#include "Thread.h"

Thread::Thread(void)
:isRunning(false)
,hThread(INVALID_HANDLE_VALUE)
{
	TRACE0("Thread\n");
}

Thread::~Thread(void)
{
	End();
	TRACE0("~Thread\n");
}

int Thread::ThreadProc(Thread *pThis)
{
	return pThis->Run();
}

bool Thread::Start(void)
{
	hThread = CreateThread(NULL, 0,
		(LPTHREAD_START_ROUTINE)ThreadProc,
		this,
		CREATE_SUSPENDED,
		NULL);

	if (hThread != INVALID_HANDLE_VALUE)
	{
        SetThreadPriority(hThread, THREAD_PRIORITY_BELOW_NORMAL);
	    ResumeThread(hThread);
	}

	return true;
}

void Thread::End(void)
{
	if (hThread != INVALID_HANDLE_VALUE)
	{
		isRunning = false;

		WaitForSingleObject(hThread, INFINITE);
		CloseHandle(hThread);
		hThread = INVALID_HANDLE_VALUE;
	}
}

bool Thread::Resume()
{
	if (hThread != INVALID_HANDLE_VALUE)
	{
	    ResumeThread(hThread);
	}

	return true;
}

bool Thread::Suspend()
{
	return true;
}
