#pragma once

#define ZM704_FLAG    0xAA55

#define M1_CARD_SECRET_CODE_A       0x60
#define M1_CARD_SECRET_CODE_B       0x61

#define M1_S50_EEPROM_MAX_SECTION 	16   // 最大扇区数
#define M1_S50_EEPROM_MAX_BLOCK 	(M1_S50_EEPROM_MAX_SECTION*4)    //  最大块数
#define M1_S50_EEPROM_BLOCK_BYTE_SIZE   16      // 每个块的字节数


#pragma pack(1)
typedef struct 
{
	unsigned short flag;
    unsigned char addr;
	unsigned short len;
	unsigned short cmd;
	unsigned char data[1];
}zm704_hdr_t;
#pragma pack()

typedef enum
{
    ZM_CMD_SCAN_CARD_AUTO = 0x0221,
	ZM_CMD_AUTHEN         = 0x0301,
	ZM_CMD_READ_BLOCK     = 0x0302,
	ZM_CMD_ENTER_CPUCARD  = 0x0401,
	ZM_CMD_SEND_COS       = 0x04F1,
}zm704_cmd_t;