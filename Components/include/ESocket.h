#pragma once

#include <afxsock.h>
// ESocket ����Ŀ��

class ESocket : public CSocket
{
public:
	ESocket();
	virtual ~ESocket();

	bool Timeout(UINT rxtm, UINT txtm);
	BOOL Connect(LPCTSTR lpszHostAddress, UINT nHostPort);
	virtual int Receive(void* lpBuf, int nBufLen, int nFlags = 0);
	virtual int Send(const void* lpBuf, int nBufLen, int nFlags = 0);

private:
	virtual BOOL OnMessagePending();

private:
	UINT_PTR m_nTimerID;
	UINT m_TxTimeout;
	UINT m_RxTimeout;
};


