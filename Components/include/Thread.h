#pragma once

class Thread
{
public:
	Thread(void);
	virtual ~Thread(void);

public:
	bool Start(void);
	void End();
	bool Resume(void);
	bool Suspend(void);

protected:
	HANDLE hThread;
	bool isRunning;
	static int ThreadProc(Thread *pThis);
	virtual int Run() = 0;
};
