#pragma once

#include "SCDev.h"

typedef enum
{
    TYPE_A = 0x60,
	TYPE_B = 0x61
}RfidCrcType;

typedef struct  
{
	unsigned short type1;
	unsigned char type2;
	unsigned int uid;
}RfidInfo;

class RFID 
{
public:
	RFID(void);
	~RFID(void);

	bool Open(const wchar_t *name, int br);
	bool Close();

    bool Alive(void);
	int CardScan(RfidInfo *info);
	bool Authen(unsigned char blkn, unsigned char type, unsigned char *pwd, short size);
    bool BlockRead(unsigned char blkn, unsigned char *buf, short size);    
	bool CpuCardMode();
	bool CosSend(unsigned char *c, short size);

    bool ReqSend(short cmd, unsigned char *data, short size);
	int AckRecv(unsigned char *buf, short size);

	void Lock();
	void Unlock();

private:
	SCDev m_CommDev;
    static int m_Refcnt;

	int Read(unsigned char *buf, short size);
	int Write(unsigned char *buf, short size);
	unsigned char Crc8(unsigned char *buf, short size);
};
