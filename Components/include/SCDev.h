#pragma once

#include <afxmt.h>

#include "SerialPort.h"

class SCDev
{
public:
	SCDev(void);
	~SCDev(void);

public:
    bool open(const wchar_t *portName, int baudRate);
    bool close();
	
	int write(unsigned char *buf, int size);
	int read(unsigned char *buf, int size);
	void lock();
	void unlock();
	void clearbuf();
	time_t rsptime();
	void setretry(int cnt);

private:
	static SerialPort Dev;
	static volatile int m_Refcnt;
	static CMutex mutex;
	static time_t RspTime;
};
