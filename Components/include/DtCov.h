#pragma once

#include <string>
using namespace std;

class DtCov
{
public:
	DtCov(void);
	~DtCov(void);

	string ftos(float &f);
	float stof(string &s);
	string wtoc(LPCWSTR w);
	string ctoutf8(string &src);
	int stoi(string &s);
	string itos(int i);
};
