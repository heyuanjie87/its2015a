#pragma once

#include "Thread.h"
#include "SCDev.h"

enum eKeyDefine
{
	KEY_11 = 'O',
	KEY_12 = 'P',
	KEY_13 = 'A',
	KEY_14 = '1',
	KEY_15 = VK_END,
	KEY_21 = 'Y',
	KEY_22 = 'U',
	KEY_23 = 'I',
	KEY_24 = '2',
	KEY_25 = 'S',
	KEY_31 = 'E',
	KEY_32 = 'R',
	KEY_33 = 'T', //'0'
	KEY_34 = '3', //VK_OEM_PERIOD, //'.',
	KEY_35 = 0, //走纸
	KEY_41 = '5',
	KEY_42 = '6',
	KEY_43 = '7',
	KEY_44 = '4',
	KEY_45 = VK_BACK,
	KEY_51 = '8',
	KEY_52 = '9',
	KEY_53 = '0',
	KEY_54 = VK_UP,
	KEY_55 = VK_TAB, 
	KEY_61 = 0xBE,
	KEY_62 = VK_HOME,
	KEY_63 = VK_ESCAPE,
	KEY_64 = VK_DOWN,
	KEY_65 = VK_RETURN,

	KEY_BACK     = KEY_15, //返回
	KEY_CANCEL   = KEY_15, //取消
	KEY_SAVE     = KEY_25, //保存
	KEY_FUNCTION = KEY_44, //功能
	KEY_MODIFY   = KEY_43, //修改
	KEY_DELETE   = KEY_45, //删除
	KEY_SMALL    = KEY_53, //小件
	KEY_UP       = KEY_54, //上
	KEY_NEXT     = KEY_55, //切换
	KEY_CALL     = KEY_61, //呼叫
	KEY_RECALL   = KEY_62, //重呼
	KEY_PRINT    = KEY_63, //打印
	KEY_DOWN     = KEY_64, //下
	KEY_WEIGHT   = KEY_65, //采重
	KEY_CONFIRM  = KEY_65, //确认
};

class Keyboard : public Thread
{
public:
	Keyboard(void);
	~Keyboard(void);

	bool Open();
	bool Close();

public:
	int GetKey(bool SendReq = false);
	bool Alive(void);

private:
    virtual int Run(void);
    void keymapInit(void);
	unsigned char Crc8(unsigned char *buf, short len);

private:
	bool Connected;
	SCDev Dev;
    BYTE keymap[256];	
};

extern Keyboard theKbd;
