#pragma once

#include "RFID.h"
#include <string>
using namespace std;

typedef struct  
{
	string Number;
	string Name;
}WorkerInfo;

class RfCardReader
{
public:
	RfCardReader(void);
	~RfCardReader(void);

	bool Open(void);
	bool Close(void);
    
	bool Alive(void);
	int CardScan(RfidInfo *rfidinfo);
	bool WorkerCardRead(WorkerInfo &wi);
	bool CreditCardRead(string &cardid);
	void Lock();
	void Unlock();

private:
    RFID m_Rfid;
	int BCD2Str(char* dst, unsigned char *src, short n);
};
