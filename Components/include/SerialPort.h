#pragma once

class SerialPort
{
public:
	SerialPort(void);
	~SerialPort(void);

	bool open(const wchar_t *portName, int baudRate);
	bool close();
	int write(unsigned char *buf, int size);
	int read(unsigned char *buf, int size);
    void clearbuf();
	void setretry(int cnt);

private:
	HANDLE hFile;
    int retry;
};
