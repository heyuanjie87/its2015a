#pragma once
#include <string>
using namespace std;

enum eLevelCode
{
	LEVEL_NULL = 0, //无效等级

	LEVEL_C1F = 1,
	LEVEL_C2F = 2,
	LEVEL_C3F = 3,
	LEVEL_C1L = 4,
	LEVEL_C2L = 5,
	LEVEL_B1F = 6,
	LEVEL_B2F = 7,
	LEVEL_B1L = 8,
	LEVEL_B1R = 9,
	LEVEL_H1F = 10,
	LEVEL_X1F = 11,
	LEVEL_C3L = 12,
	LEVEL_C4F = 13,
	LEVEL_C4L = 14,
	LEVEL_X2F = 15,
	LEVEL_X3F = 16,
	LEVEL_X1L = 17,
	LEVEL_X2L = 18,
	LEVEL_B3F = 19,
	LEVEL_B4F = 20,
	LEVEL_B2L = 21,
	LEVEL_B3L = 22,
	LEVEL_B2R = 23,
	LEVEL_B3R = 24,
	LEVEL_H2F = 25,
	LEVEL_X2V = 26,
	LEVEL_C3V = 27,
	LEVEL_B2V = 28,
	LEVEL_B3V = 29,
	LEVEL_S1 = 30,
	LEVEL_B4L = 31,
	LEVEL_X3L = 32,
	LEVEL_X4L = 33,
	LEVEL_X4F = 34,
	LEVEL_S2 = 35,
	LEVEL_CX1K = 36,
	LEVEL_CX2K = 37,
	LEVEL_B1K = 38,
	LEVEL_B2K = 39,
	LEVEL_B3K = 40,
	LEVEL_GY1 = 41,
	LEVEL_GY2 = 42,
	LEVEL_JW  = 43,

	LEVEL_MAX = 44,
};

class TobLevel
{
public:
	TobLevel(void);
	~TobLevel(void);

	void Reset();
	bool Completed();
	void Input(int key);
	wchar_t* Get();
	static bool IsLevelKey(int key);
    int GetId(wchar_t *nm);
	void GetId(wchar_t *nm, string &id);
	static wchar_t* Get(string &id);

private:
	int m_NumKey; //输入按键的数量
	int m_Index;
	bool m_Comp;
    wchar_t *m_name;
	int m_LvId;
};


