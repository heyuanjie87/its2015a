#pragma once

#include "Thread.h"

#include <iostream>
#include <queue>
#include <string>
#include <xstring>
using namespace std;

class SoundPlay : public Thread
{
public:
	SoundPlay(void);
	~SoundPlay(void);

protected:
    virtual int Run();

public:

	void Open();
	void Close();

	void PlayNumber(int val);
	void PlayNumber(float val);
	void PlayLevel(wstring &lv);
	void PlayWeight(wstring &lv, float val);
	void PlayWeight(string &lv, float w);
	void PlayTips(WCHAR *s);

public:
	void weightinfo(int level, float weight);
	void welcome();
	void carToLocation( const std::wstring& car, const std::wstring& location );

	void playString( const std::wstring& str );
	void playNumberStr( const char* weight, int digit );
	void playFile(const wstring &name);
	void playAsync( const std::wstring& name );
	void playSound( const wchar_t* name );
	void clear();
	void setSoundPath(const wstring &path);
	void playLevel( int level );
private:
	const wchar_t *getLevelSoundLocation(int level);

	void snd_integer(int num, int digit);
	void snd_playweight(const char *weight);
	bool getNextSoundFile( std::wstring& file );

	void PlayFile(wstring &path, WCHAR *name);

private:
    queue<wstring> sndFiles;
    wstring soundPath;
};

extern SoundPlay theSndPlay;
