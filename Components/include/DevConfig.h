#pragma once

#include <string>
#include <xstring>
using namespace std;

class DevConfig
{
public:
	DevConfig(void);
	~DevConfig(void);

	void RebindNdis(LPCWSTR strAdapterName);
	CString GetLocalIp(LPCWSTR strAdapterName);
	BOOL SetIpConfig(LPCWSTR strAdapterName, LPCWSTR m_StrIPAddress, LPCWSTR m_StrIPMask, LPCWSTR m_StrGate, DWORD useDHCP);
    BOOL GetIpConfig(LPCWSTR strAdapterName, LPWSTR m_StrIPAddress, LPWSTR m_StrIPMask, LPWSTR m_StrGate, DWORD& useDHCP);
    BOOL GetServerIp(LPWSTR ip, LPWSTR port);
    BOOL SetServerIp(LPCWSTR ip, LPCWSTR port);
	BOOL GetServerIp(CString &ip, int &port);
	BOOL GetDevSn(string &sn);
	BOOL GetDevSn(CString &sn);
	BOOL GetComKeyboard(WCHAR *buf, int size);
	BOOL GetComScale(WCHAR *buf, int size);
	void SetComKeyboard(LPCWSTR buf);
	void SetComScale(LPCWSTR buf);

	BOOL IsIP(CString &s);

private:
    BOOL CreateReg(WCHAR *key, LPCWSTR val);
	BOOL GetReg(WCHAR *key, WCHAR *buf, DWORD dwStrLen);
};
