// AcpLineSel.cpp : 实现文件
//

#include "stdafx.h"
#include "its2015.h"
#include "AcpLineSel.h"


// AcpLineSel 对话框

IMPLEMENT_DYNAMIC(AcpLineSel, CDialog)

AcpLineSel::AcpLineSel(CWnd* pParent /*=NULL*/)
	: CDialog(AcpLineSel::IDD, pParent)
{

}

AcpLineSel::~AcpLineSel()
{
}

void AcpLineSel::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_Lines);
}

INT_PTR AcpLineSel::Start(vector <string> &lines)
{
	int size;

	size = (int)lines.size();
	if (size == 0)
		return IDCANCEL;

	m_sLine = lines;

	return CDialog::DoModal();
}

void AcpLineSel::GetSel(string &sel)
{
    sel = m_sLine[m_sel];
}

void AcpLineSel::OnOK()
{
	UpdateData(TRUE);

    m_sel = m_Lines.GetSelectionMark();
    m_sel = (int)m_Lines.GetItemData(m_sel);

	CDialog::OnOK();
}

BOOL AcpLineSel::OnInitDialog()
{
	CDialog::OnInitDialog();
	int size;
    CString str;

	size = (int)m_sLine.size();

	for (int i = 0; i < size; i ++)
	{
		str = m_sLine[i].c_str();
		m_Lines.InsertItem(0, str);
        m_Lines.SetItemData(0, i);
	}

	m_Lines.SetFocus();
	m_Lines.SetItemState(0, LVIS_FOCUSED | LVIS_SELECTED, LVIS_FOCUSED | LVIS_SELECTED);
	m_Lines.SetSelectionMark(0);

	return TRUE;
}

BEGIN_MESSAGE_MAP(AcpLineSel, CDialog)
END_MESSAGE_MAP()


// AcpLineSel 消息处理程序
