#pragma once
#include "afxwin.h"


// ComCfg 对话框

class ComCfg : public CDialog
{
	DECLARE_DYNAMIC(ComCfg)

public:
	ComCfg(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~ComCfg();

// 对话框数据
	enum { IDD = IDD_DIALOG_COMCFG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
    virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
	CComboBox m_Scale;
	CComboBox m_Kbd;
};
