// ComCfg.cpp : 实现文件
//

#include "stdafx.h"
#include "its2015.h"
#include "ComCfg.h"
#include "DevConfig.h"


// ComCfg 对话框

IMPLEMENT_DYNAMIC(ComCfg, CDialog)

ComCfg::ComCfg(CWnd* pParent /*=NULL*/)
	: CDialog(ComCfg::IDD, pParent)
{

}

ComCfg::~ComCfg()
{
}

void ComCfg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_Scale);
	DDX_Control(pDX, IDC_COMBO2, m_Kbd);
}

BOOL ComCfg::OnInitDialog()
{
    CDialog::OnInitDialog();

	DevConfig cfg;
    WCHAR buf[12];
	int sel;

	for (int i = 1; i < 15; i ++)
	{
        CString com;

		com.Format(_T("COM%d"), i);
        m_Kbd.AddString(com);
		m_Scale.AddString(com);
	}

	if (!cfg.GetComKeyboard(buf, 12))
	{
        cfg.SetComKeyboard(buf);
	}
	sel = buf[3] - '1';
	m_Kbd.SetCurSel(sel);

	if (!cfg.GetComScale(buf, 12))
	{
		cfg.SetComScale(buf);
	}

	sel = buf[3] - '1';
    m_Scale.SetCurSel(sel);

	UpdateData(FALSE);

	return TRUE;
}


BEGIN_MESSAGE_MAP(ComCfg, CDialog)
	ON_BN_CLICKED(IDOK, &ComCfg::OnBnClickedOk)
END_MESSAGE_MAP()


// ComCfg 消息处理程序

void ComCfg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
    UpdateData(TRUE);

    CString selStr;
    DevConfig cfg;
    int sel;

    sel = m_Kbd.GetCurSel();
	m_Kbd.GetLBText(sel, selStr);
    cfg.SetComKeyboard((LPCWSTR)selStr);

	sel = m_Scale.GetCurSel();
	m_Scale.GetLBText(sel, selStr);
    cfg.SetComScale((LPCWSTR)selStr);

	OnOK();
}
