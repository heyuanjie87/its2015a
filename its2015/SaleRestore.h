#pragma once


// SaleRestore 对话框

class SaleRestore : public CDialog
{
	DECLARE_DYNAMIC(SaleRestore)

public:
	SaleRestore(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~SaleRestore();

// 对话框数据
	enum { IDD = IDD_DIALOG_RESTORE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
};
