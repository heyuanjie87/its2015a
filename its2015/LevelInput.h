#pragma once
#include "TbLvEdit.h"

// LevelInput 对话框

class LevelInput : public CDialog
{
	DECLARE_DYNAMIC(LevelInput)

public:
	LevelInput(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~LevelInput();

	virtual void OnOK();
	virtual BOOL OnInitDialog();

	INT_PTR Start(int key = 0);
    
	bool IsDone();
    CString GetLevel(bool clr);

// 对话框数据
	enum { IDD = IDD_DIALOG_SORT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

private:
	TbLvEdit m_InputEdit;
public:
	CString m_Level;
};
