// DevSet.cpp : 实现文件
//

#include "stdafx.h"
#include "its2015.h"
#include "DevSet.h"
#include "ServerIp.h"
#include "MainFrm.h"
#include "ComCfg.h"

// DevSet

IMPLEMENT_DYNCREATE(DevSet, CFormView)

DevSet::DevSet()
	: CFormView(DevSet::IDD)
{

}

DevSet::~DevSet()
{
}

void DevSet::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL DevSet::PreTranslateMessage(MSG *pMsg)
{
    if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_HOME)
		{
			CMainFrame* pmf = (CMainFrame*)(AfxGetApp()->m_pMainWnd);

			pmf->SwitchView(IDD_VIEW_FUCSEL);

			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN)
		{
			return TRUE;
		}

		if (pMsg->wParam == '1')
		{
			OnBnClickedButton2();

			return TRUE;
		}

		if (pMsg->wParam == '2')
		{
			OnBnClickedButton3();

			return TRUE;
		}
	}

	return CFormView::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(DevSet, CFormView)
	ON_BN_CLICKED(IDC_BUTTON2, &DevSet::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &DevSet::OnBnClickedButton3)
END_MESSAGE_MAP()


// DevSet 诊断

#ifdef _DEBUG
void DevSet::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void DevSet::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// DevSet 消息处理程序

void DevSet::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	ServerIp dlg;

	dlg.DoModal();
}

void DevSet::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	ComCfg dlg;

	dlg.DoModal();
}
