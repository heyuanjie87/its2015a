#pragma once

#include "dtype.h"

#include <string>
using namespace std;

#include "RfCardReader.h"
#include "LevelInput.h"
#include "ElecScale.h"

// WeightView 窗体视图
typedef enum
{
	ESS_OK,       //正常
	ESS_NCONNECT, //未连接
	ESS_NZERO,    //未归零
	ESS_OTARE,    //超皮重
}EsStatus;

typedef enum
{
	ST_BEGIN,
	ST_SORT_DONE,
	ST_FARM_CARD_START,
	ST_FARM_CARD_END,
	ST_DONE,
	ST_SCALE_NZERO,
	ST_SCALE_ZERO,
	ST_WOVER_CONT,
	ST_WOVER_TODAY,
}SoundTips;

typedef enum
{
	STAGE_INIT         = 0,  //初始化参数
	STAGE_WAIT_FARM       ,  //等待烟农刷卡
	STAGE_QUERY_ORDER     ,  //查询订单信息
	STAGE_READY           ,  //准备就绪
	STAGE_ONLY_SORT       , //仅定级完成
	STAGE_COMP            , //定级磅码都完成
	STAGE_WAIT_CONFIRM    ,  //等待烟农确认
	STAGE_SEND_CONFIRM    ,  //发送确认消息
	STAGE_WAIT_SAVE       ,  //等待操作员保存提交数据
	STAGE_FINAL           , //过程结束

	STAGE_WAIT_CHECK  = 100,
	STAGE_RESTORE     = 101,
	STAGE_UNFINISHED  = 102,
}WorkStage;

class WeightView : public CFormView
{
	DECLARE_DYNCREATE(WeightView)

public:
	WeightView();           // 动态创建所使用的受保护的构造函数
	virtual ~WeightView();

public:
	enum { IDD = IDD_VIEW_WEIGHT };




#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

public:
	void ShowStatus(string &sortsn, WeightStat s);
	void ShowWeight(float nw, float tw);
	void ShowWeightOk(string &sortsn, float nw, float tw);
	void ShowSortExcep(int sn, string &tip);
	void ShowTime();
	void ShowTips(string &msg);
	void ShowTips(const char *msg, int sleep = 0);
	void SwitchView();
	void HandleMessages(MSG &msg, int &stage);
    bool ReadCard(RfCardReader &reader, string &cardid, int *err = NULL);
	void SetWorkStage(WorkStage ws);
    bool CheckCont(DtRspCont &ci);
    void ShowCont(DtRspCont &ci);
    void ReInit();
	EsStatus CheckElecScale(ElecScale &es, float td, float t, float tu);
    void CheckCallDelay();
	void PlayTips(SoundTips st);
    void ClearDisplay();
	void ShowContType(string &c, string &b);
    void ShowScaleNum(string &n);
    void ClearData();
	void SetZero(ElecScale &es);

public:
	bool SqlSaveCont(DtRspCont &cont);
    bool SqlAddWList(WeightList &wl);
	bool SqlSetWList(WeightList &wl);
	bool SqlGetCont(DtRspCont &cont);

public:
	bool GetContract(string &cardid, DtRspCont &rsp);
    WeightStat HandWeightOk(DtReqWeight &req, int sn, float nw, float tw);
    bool FarmerConfirm(string &cardid);
	bool OperatorConfirm();
    int Restore();

private:
	void AddWeightList(CString &lv);
    string GetTime();
    void HandSorting(int sn, int key);
    void HandWeightDel(int sn, int sel);
    void HandEndSort();
	void HandCall(int type);
	bool HandContDel(void);
	void HandWeightCancel(int sn);
	int GetSelSn();
	string GetSaleId();

	void Exit();

private:
	static DWORD WorkerThread(LPVOID lParam);
    LevelInput m_TbInput;
	bool m_CallEnable;
	int m_CallDelay;
	bool m_ContLock; //锁定合同不许删除
	bool m_SortLock; //锁定后不允许继续定级
	CListCtrl m_TobList;
    CImageList m_imgList;
	int m_ItemClr;

public:
	bool isRun;
	WorkStage m_WrkStage;
	int m_TbSn;
	MSG m_LastMsg;
    bool m_Retry;
    CWinThread *m_Thread;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
    virtual BOOL PreTranslateMessage(MSG *pMsg);
    virtual void OnInitialUpdate();

	DECLARE_MESSAGE_MAP()

private:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
    afx_msg void OnLvnItemchangedListTob(NMHDR *pNMHDR, LRESULT *pResult);

private:
	bool KeyLevelInput(int key);
	void KeyOk();
	void KeyDelete();
	void KeyEnd();
	void KeyHome();
    void MsgPost(UINT type, WPARAM wp = 0, LPARAM lp = 0);

public:
	afx_msg void OnDestroy();
	afx_msg void OnNMClickListTob(NMHDR *pNMHDR, LRESULT *pResult);
};


