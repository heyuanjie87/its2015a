#pragma once

#include <vector>
#include "afxcmn.h"
using namespace std;

// AcpLineSel 对话框

class AcpLineSel : public CDialog
{
	DECLARE_DYNAMIC(AcpLineSel)

public:
	AcpLineSel(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~AcpLineSel();

// 对话框数据
	enum { IDD = IDD_DIALOG_SEL };

	INT_PTR Start(vector <string> &lines);
    void GetSel(string &sel);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
    virtual BOOL OnInitDialog();
    virtual void OnOK();

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_Lines;

private:
	vector <string> m_sLine;
	int m_sel;
};
