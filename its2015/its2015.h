// its2015.h : its2015 应用程序的主头文件
//
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"       // 主符号


// Cits2015App:
// 有关此类的实现，请参阅 its2015.cpp
//

class Cits2015App : public CWinApp
{
public:
	Cits2015App();


// 重写
public:
	virtual BOOL InitInstance();
    CString GetPath();

// 实现
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern Cits2015App theApp;