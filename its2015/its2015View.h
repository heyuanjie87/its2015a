// its2015View.h : Cits2015View 类的接口
//


#pragma once


class Cits2015View : public CView
{
protected: // 仅从序列化创建
	Cits2015View();
	DECLARE_DYNCREATE(Cits2015View)

// 属性
public:
	Cits2015Doc* GetDocument() const;

// 操作
public:

// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// 实现
public:
	virtual ~Cits2015View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // its2015View.cpp 中的调试版本
inline Cits2015Doc* Cits2015View::GetDocument() const
   { return reinterpret_cast<Cits2015Doc*>(m_pDocument); }
#endif

