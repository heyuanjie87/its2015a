#pragma once



// FucSel 窗体视图

class FucSel : public CFormView
{
	DECLARE_DYNCREATE(FucSel)

public:
	FucSel();           // 动态创建所使用的受保护的构造函数
	virtual ~FucSel();

public:
	enum { IDD = IDD_VIEW_FUCSEL };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
    virtual BOOL PreTranslateMessage(MSG *pMsg);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton3();
	afx_msg void OnBnClickedButton2();
};


