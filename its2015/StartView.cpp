// StartView.cpp : 实现文件
//

#include "stdafx.h"
#include "its2015.h"
#include "StartView.h"
#include "SoundPlay.h"
#include "CSIntf.h"
#include "DevConfig.h"
#include "MainFrm.h"
// StartView
#include "WorkParam.h"
#include "AcpLineSel.h"
#include "version.h"
#include "Keyboard.h"
#include "DataPkt.h"
#include "DtCov.h"
#include "SqlHelper.h"

IMPLEMENT_DYNCREATE(StartView, CFormView)

StartView::StartView()
	: CFormView(StartView::IDD)
{
    isRun = false;
	m_RetryCnt = 0;
	m_Retry = false;
	m_ErrCode = 0;
}

StartView::~StartView()
{
	if (isRun)
	{
		isRun = false;
		WaitForSingleObject(hThread, INFINITE);
	}

	CloseHandle(hThread);
}

BOOL StartView::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_RETURN)
		{
			if (m_Retry)
			{
				m_Retry = false;
				m_RetryCnt = 0;
			}

			return TRUE;
		}
	}

	return CFormView::PreTranslateMessage(pMsg);
}

void StartView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, _LoginMsg);
}

void StartView::SqlInit()
{
	CString path;
	string spath;
	DtCov cov;

	path = theApp.GetPath();
	spath = cov.wtoc((LPCWSTR)path);
	spath += "\\db\\";

	if (!theSql.Init(spath.c_str(), "its2015.db"))
	{
		AfxMessageBox(_T("初始化数据库失败"));
		return;
	}

	theSql.InsertParam(theWkParam.m_InitParam);
	theSql.SearchParam(theWkParam.m_InitParam);
}

bool StartView::DevLogin(DtRspDevLogin &rsp)
{
    bool ret = false;
	DtReqDevLogin req;

    if (m_RetryCnt == 3)
	{
        ShowTips("设备登录失败，按【确认】重试");
		
		m_Retry = true;
        return ret;
	}

	req.SwVersion = SW_VERSION;
	req.HwVersion = HW_VERSION;
    req.StationCode = theWkParam.m_InitParam.StationCode;

    CSIntf inf;

	ShowTips("设备登录");
	ret = inf.DevLogin(req, rsp);

	if (!ret)
	{
        ShowTips(inf.GetErr());
        m_RetryCnt ++;
	}
	else
	{
		if (rsp.ErrCode != "0")
		{
            ShowTips(rsp.ErrMsg);
			m_RetryCnt ++;
			return false;
		}
	}

	return ret;
}

union SelfTst
{
	UINT8 val;
	struct  
	{
		UINT8 Net:1;
		UINT8 RfCard:1;
		UINT8 Kbd:1;
	};
};

bool StartView::WorkerLogin(DtWorker &worker, bool &finish)
{
	bool ret = false;
    CSIntf inf;
	DtRspWkLogin rsp;

	if (m_RetryCnt == 3)
	{
		ShowTips("工作员登录失败，按【确认】重试");

		m_Retry = true;
		return ret;
	}

	ShowTips("工作员登录");

	ret = inf.WorkerLogin(worker, rsp);
	if (!ret)
	{
		ShowTips(inf.GetErr());
		m_RetryCnt ++;
	}
	else
	{
        ShowWokerLogin(rsp);
	}

    finish = rsp.IsFinish;

	return ret;
}

bool StartView::GetJobs(DtRspJobs &job)
{
	bool ret = false;

	if (m_RetryCnt == 3)
	{
		ShowTips("获取岗位失败，按【确认】重试");
		m_Retry = true;
		return ret;
	}

	CSIntf inf;

	ShowTips("获取岗位列表");

	ret = inf.GetJobs(job);
	if (!ret)
	{
		ShowTips(inf.GetErr());
		m_RetryCnt ++;      
	}
	else
	{
		for (int i = 0; i < (int)job.JobList.size(); i ++)
		{
            ShowJobs(job.JobList[i]);
		} 
	}

	return ret;
}

bool StartView::GetParam(DtRspParam &rsp)
{
	bool ret = false;
	CSIntf inf;

	if (m_RetryCnt == 3)
	{
		ShowTips("下载参数失败，按【确认】重试");
		m_Retry = true;
		return ret;
	}

	ShowTips("下载初始参数");

	ret = inf.GetInitParam(rsp);
	if (!ret)
	{
		ShowTips(inf.GetErr());
		m_RetryCnt ++;      
	}
	else
	{
		theSql.UpdateParam(theWkParam.m_InitParam);
	}

	return ret;
}

bool StartView::ReadCard(RfCardReader &reader, DtWorker &worker)
{
	bool ret = false;
	RfidInfo rfinfo;
	WorkerInfo winfo;

	ShowTips("读卡中...");

	if (!reader.CardScan(&rfinfo))
		goto EXIT;

	if (!reader.WorkerCardRead(winfo))
		goto EXIT;

    worker.Name = winfo.Name;
    worker.Id = winfo.Number;

	ShowTips("读卡成功");

EXIT:

	return ret;
}

void StartView::ShowTips(const char *msg)
{
    if (!isRun)
		return;

	CString str;

	str = msg;

    SetDlgItemText(IDC_STATIC_TIPS, str);
}

void StartView::ShowTips(string &msg)
{
	if (!isRun)
		return;

	CString str;

	str = msg.c_str();

	SetDlgItemText(IDC_STATIC_TIPS, str);
}

void StartView::ShowJobs(DtJob &job)
{
	if (!isRun)
		return;

	CString str;

	str = job.Name.c_str();
	_LoginMsg.InsertItem(0, str);
	_LoginMsg.SetItemText(0, 1, _T("请刷卡"));
    _LoginMsg.SetItemData(0, job.Code);
}

void StartView::SetTime(string &t)
{
    if (m_RemoteIp == m_LocalIp)
		return;

	SYSTEMTIME st;
	CString str;

	str = t.c_str();

	COleVariant vtime(str);   
	vtime.ChangeType(VT_DATE);   
	COleDateTime oletime = vtime;   

	VariantTimeToSystemTime(oletime, &st);

	SetLocalTime(&st);
}

void StartView::SwitchView()
{
	CMainFrame* pmf = (CMainFrame*)(AfxGetApp()->m_pMainWnd);

	pmf->SwitchView(IDD_VIEW_FUCSEL);
}

bool StartView::AcqLineSel()
{
    AcpLineSel dlg(this);
    CSIntf inf;
    bool ret = false;
	DtRspAcqLine rsp;

	if (m_RetryCnt == 3)
	{
		ShowTips("绑定收购线失败，按【确认】重试");

		m_Retry = true;
		return ret;
	}
    
	ShowTips("获取收购线");

	ret = inf.GetAcqLine(rsp);
	if (!ret)
	{
		ShowTips(inf.GetErr());
		m_RetryCnt ++;
		return false;
	}
	else
	{
		if (rsp.ErrCode != "0")
		{
			ShowTips(rsp.ErrMsg);
			m_RetryCnt ++;
			return false;
		}

		if (dlg.Start(rsp.Lines) != IDOK)
		{
            m_RetryCnt = 3;
            return false;
		}	
	}

	ShowTips("设定收购线");

    string sel;

	dlg.GetSel(sel);

	ret = inf.SetAcqLine(sel, rsp);
	if (!ret)
	{
		ShowTips(inf.GetErr());
		m_RetryCnt ++;
		return false;
	}
	else
	{
		if (rsp.ErrCode != "0")
		{
			ShowTips(rsp.ErrMsg);
			m_RetryCnt ++;
			return false;
		}
	}

	return ret;
}

BEGIN_MESSAGE_MAP(StartView, CFormView)
    ON_MESSAGE(WM_CFG_CHANGED, &StartView::OnCfgChanged)
END_MESSAGE_MAP()


// StartView 诊断

#ifdef _DEBUG
void StartView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void StartView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

DWORD StartView::WorkerThread(LPVOID lParam)
{
    StartView *dlg;
	int stage = 1;
	DtRspJobs job;
	RfCardReader card;
	SelfTst devstat;

	dlg = (StartView *)lParam;
	dlg->isRun = true;
    devstat.val = 0x07;

	theSndPlay.welcome();
	AfxSocketInit();

	dlg->ShowDevInfo();

    card.Open();

	dlg->SqlInit();

	while (dlg->isRun)
	{

        switch (stage)
		{
		case 0: //自检
		{
			if (theKbd.GetKey() >= 0)
			{
				dlg->ShowTips("定级键盘正常");
				devstat.Kbd = 0;
			}
			else
			{
				dlg->ShowTips("未发现定级键盘");
				Sleep(1000);
			}

			if (card.Alive())
			{
				dlg->ShowTips("刷卡模块正常");
				devstat.RfCard = 0;
			}
			else
			{
				dlg->ShowTips("未发现读卡器");
			}

			if (devstat.val == 0)
			{
				stage ++;
			}
		}
		break;
		case 1: //设备登录
		{
			DtRspDevLogin rsp;

            if (dlg->DevLogin(rsp))
			{
				if (rsp.ErrCode == "0")
				{
					stage = 5;
				}
				else
				{
                    stage = 2;
				}
			}
		}
		break;
		case 2: //选择收购线
		{
			if (dlg->AcqLineSel())
			{
				stage ++;
			}
		}
		break;
		case 3: //获取岗位列表
		{
			if (dlg->GetJobs(job))
			{
				stage ++;
			}
		}
		break;
		case 4:
		{
			DtWorker worker;
#if 0
			if (!card.Alive())
			{
				dlg->ShowTips("刷卡模块异常");
				break;
			}

			if (!dlg->ReadCard(card, worker))
			{
				break;
			}
#else
            worker.Id = "7212345";
			worker.Name = "测试";
#endif
			bool finish;

			if (!dlg->WorkerLogin(worker, finish))
				break;

			if (finish)
			{
				stage ++;
			}
		}
		break;
		case 5:
		{
            DtRspParam &param = theWkParam.m_InitParam;

            if (dlg->GetParam(param))
			{
				stage ++;
			}
		}
		break;
		default:
			goto EXIT;
		}

		Sleep(1000);
	}

EXIT:
    card.Close();

	if (dlg->isRun)
	{
		dlg->isRun = false;
		dlg->SwitchView();
	}

	return 0;
}

#include <io.h>
#include <fcntl.h>
#include <stdio.h>

void InitConsoleWindow()
{
	int nCrt = 0;
	FILE* fp;
	AllocConsole();
	nCrt = _open_osfhandle((intptr_t)GetStdHandle(STD_OUTPUT_HANDLE), _O_TEXT);
	fp = _fdopen(nCrt, "w");
	*stdout = *fp;
	setvbuf(stdout, NULL, _IONBF, 0);
}

void StartView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();

//#ifdef _DEBUG
    InitConsoleWindow();
//#endif

	hThread = CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE)WorkerThread, (LPVOID)this,CREATE_SUSPENDED,NULL);

	//设置优先级为低于正常
	SetThreadPriority(hThread, THREAD_PRIORITY_BELOW_NORMAL);
	ResumeThread(hThread);

	CRect rect;

	_LoginMsg.GetClientRect(&rect);
	_LoginMsg.InsertColumn(0, _T(""), LVCFMT_LEFT, rect.Width()/2, 0);   
	_LoginMsg.InsertColumn(1, _T(""), LVCFMT_LEFT, rect.Width()/2, 0); 
}

void StartView::ShowDevInfo()
{
	CString s, port;
	DevConfig ipc;
	CString m_SwVer;
	CString m_DevSn;
	int m_RemotrPort;

	m_LocalIp = ipc.GetLocalIp(L"DM9CE1");
	m_SwVer = SW_VERSION;
	ipc.GetDevSn(m_DevSn);

	if (ipc.GetServerIp(m_RemoteIp, m_RemotrPort) == FALSE)
	{
		m_RemoteIp = _T("192.168.255.128");
		m_RemotrPort = 5555;
	}

	port.Format(_T(":%d"), m_RemotrPort);
	s = m_RemoteIp + port;
    
	CSIntf::SetServerAddr(m_RemoteIp, m_RemotrPort);
	
	string sn;
    ipc.GetDevSn(sn);
	DataPkt::DevInfoSet(sn);

	SetDlgItemText(IDC_STATIC_LOCALIP, m_LocalIp);
	SetDlgItemText(IDC_STATIC_REMOTEIP, s);
	SetDlgItemText(IDC_STATIC_SWVER, m_SwVer);
	SetDlgItemText(IDC_STATIC_DEVSN, m_DevSn);
}

void StartView::ShowWokerLogin(DtRspWkLogin &rsp)
{
	if (!isRun)
		return;

	CString str;

	for (int i = 0; i < _LoginMsg.GetItemCount(); i ++)
	{
		if (rsp.JobCode == _LoginMsg.GetItemData(i))
		{
            str = _T("已登录");
            _LoginMsg.SetItemText(i, 1, str);
		}
	}
}

// StartView 消息处理程序
LRESULT StartView::OnCfgChanged(WPARAM wParam, LPARAM lParam)
{
    ShowDevInfo();

	return NULL;
}
