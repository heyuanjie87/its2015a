#pragma once
#include "afxwin.h"

#include "dtype.h"
#include "RfCardReader.h"

#include <string>
#include "afxcmn.h"
using namespace std;

// StartView 窗体视图

class StartView : public CFormView
{
	DECLARE_DYNCREATE(StartView)

public:
	StartView();           // 动态创建所使用的受保护的构造函数
	virtual ~StartView();

public:
	enum { IDD = IDD_VIEW_START };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif
    bool isRun;
	int m_RetryCnt;
	bool m_Retry;
	int m_ErrCode;

public:
	bool DevLogin(DtRspDevLogin &rsp);
	bool WorkerLogin(DtWorker &worker, bool &finish);
	bool GetParam(DtRspParam &rsp);
	bool ReadCard(RfCardReader &reader, DtWorker &worker);
	bool GetJobs(DtRspJobs &job);
	bool AcqLineSel();

	void ShowTips(string &msg);
	void ShowTips(const char *msg);
    void ShowJobs(DtJob &job);
    void ShowDevInfo();
	void ShowWokerLogin(DtRspWkLogin &rsp);

	void SwitchView();

	void SetTime(string &t);
	void SqlInit();

private:
	static DWORD WorkerThread(LPVOID lParam);

private:
	HANDLE hThread;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
    virtual void OnInitialUpdate();
	virtual BOOL PreTranslateMessage(MSG *pMsg);
    afx_msg LRESULT OnCfgChanged(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
private:
	CString m_LocalIp;
	CString m_RemoteIp;
public:
	CListCtrl _LoginMsg;
};


