// WeightView.cpp : 实现文件
//

#include "stdafx.h"
#include "its2015.h"
#include "WeightView.h"
#include "MainFrm.h"
#include "DtCov.h"
#include "CSIntf.h"
#include "usermsg.h"
// WeightView
#include "WorkParam.h"
#include "SoundPlay.h"
#include "SqlHelper.h"
#include "SaleRestore.h"

IMPLEMENT_DYNCREATE(WeightView, CFormView)

WeightView::WeightView()
	: CFormView(WeightView::IDD)
{
    isRun = false;
}

WeightView::~WeightView()
{

}

void WeightView::Exit()
{
	if (isRun)
	{
		isRun = false;
		WaitForSingleObject(m_Thread->m_hThread, 2000);

	}
}

void WeightView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_LIST_TOB, m_TobList);
}

void WeightView::MsgPost(UINT type, WPARAM wp, LPARAM lp)
{
	m_Thread->PostThreadMessage(type, wp, lp);
}

BOOL WeightView::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		//确认
		if (pMsg->wParam == VK_RETURN)
		{
			KeyOk();

			return TRUE;
		}

		//返回
		if (pMsg->wParam == VK_HOME)
		{
            KeyHome();
			return TRUE;
		}

		//呼叫
		if (m_CallEnable)
		{
			if (pMsg->wParam == '1' || pMsg->wParam == '2')
			{
				m_CallEnable = false;
				MsgPost(WM_CALL, pMsg->wParam);

                return TRUE;
			}
		}

		//屏蔽数字键
		if (pMsg->wParam == '1' || pMsg->wParam == '2' || pMsg->wParam == '3' || pMsg->wParam == '4')
		{
            return TRUE;
		}

		//保存
		if (pMsg->wParam == 'S')
		{
			if (m_WrkStage == STAGE_WAIT_SAVE)
			{
				MsgPost(WM_SAVE);
			}

			return TRUE;
		}

		//定级
		if (m_WrkStage == STAGE_READY)
		{
            if (KeyLevelInput((int)pMsg->wParam))
				return TRUE;
		}
        //结束
		if (pMsg->wParam == VK_END)
		{
            KeyEnd();
			return TRUE;
		}

		//删除
		if (pMsg->wParam == VK_BACK)
		{
            KeyDelete();
			return TRUE;
		}

		//取消
		if (pMsg->wParam == VK_ESCAPE)
		{
			int sn;

			sn = GetSelSn();

			MsgPost(WM_WEIGHT_CANCEL, sn);
			return TRUE;
		}
	}

    return CFormView::PreTranslateMessage(pMsg);
}

int WeightView::GetSelSn()
{
	int sn = 0;
	int sel;

	sel = m_TobList.GetSelectionMark();
	if (sel >= 0)
	{
		CString csn;

		csn = m_TobList.GetItemText(sel, 0);
		sn = _ttoi(csn);
	}

	return sn;
}

void WeightView::ClearData()
{
	theWkParam.Clear();
	theSql.Clear();
}

void WeightView::KeyHome()
{
	if ( m_WrkStage == STAGE_WAIT_FARM)
	{    
		ShowTips("正在退出交售");

		MsgPost(WM_RETURN);
	}
	else if (m_WrkStage == STAGE_ONLY_SORT)
	{
		//在定级后取消交售
		MsgPost(WM_CONT_DEL);
	}
}

void WeightView::KeyEnd()
{
	if (m_ContLock)
	{
		//已经定级
		if (m_WrkStage < STAGE_WAIT_CONFIRM)
		{
			MsgPost(WM_WEIGHT_FINISH);
		}
	}
	else
	{
		//在刷卡后取消交售
		if (m_WrkStage == STAGE_READY)
		{
			MsgPost(WM_CONT_DEL);
		}
	}
}

void WeightView::KeyDelete()
{
	if (m_WrkStage == STAGE_READY || m_WrkStage == STAGE_ONLY_SORT)
	{
		int sel;

		sel = m_TobList.GetSelectionMark();
		if (sel >= 0)
		{
			CString csn;
			int sn;

			csn = m_TobList.GetItemText(sel, 0);
			sn = _ttoi(csn);
			MsgPost(WM_WEIGHT_DEL, sn, sel);
		}
	}
}

void WeightView::KeyOk()
{
	int sel;
    
	//通信失败重试
	if (m_Retry)
	{
        MsgPost(m_LastMsg.message, m_LastMsg.wParam, m_LastMsg.lParam);
		return;
	}
    //回到初始流程
	if (m_WrkStage == STAGE_FINAL)
	{
		MsgPost(WM_RESTART, 0, 0);
		return;
	}
    //称重
	sel = m_TobList.GetSelectionMark();
	if (sel >= 0)
	{
		CString val;
		int sn;

		val = m_TobList.GetItemText(sel, 0);
		sn = _ttoi(val);

        val = m_TobList.GetItemText(sel, 4);
		if (val != _T(" "))//是否称重标记
		{
            MsgPost(WM_WEIGHT_OK, sn, sel);
		}
	}
}

bool WeightView::KeyLevelInput(int key)
{
    bool ret = false;

	if (TobLevel::IsLevelKey(key))
	{
		if (m_TbSn > 100)
		{
			ShowTips("烟筐数已达上限");
			return true;
		}

		ShowTips("按【删除】键重输等级,按【确认】键确定等级,按【取消】键放弃输入");

		INT_PTR nResponse = m_TbInput.Start(key);

		if (nResponse == IDOK)
		{
			if (m_TbInput.IsDone())
			{
				if (m_SortLock)
				{
					ShowTips("上次定级未完成");
					return true;
				}

				CString val;

				m_ContLock = true;
				m_SortLock = true;
				val = m_TbInput.GetLevel(true);

				AddWeightList(val);    
			}
		}

		ret = true;
	}

	return ret;
}

bool WeightView::HandContDel()
{
	bool ret = false;
	WListStatus wls;

	wls = theWkParam.CheckWList();

	if (wls == WLS_DLE || wls == WLS_NULL)
	{
		int sel;

		sel = MessageBox( L"按【确认】键取消交售, 按【取消】键继续交售", L"是否取消该烟农交烟?", MB_OKCANCEL);
		if (sel == 1)
		{
			if (OperatorConfirm())
			{
                PlayTips(ST_DONE);
				ret = true;
			}
		}
	}
	else
	{
		ShowTips("删除全部烟框后才能取消交售");
		Sleep(2000);
	}

    return ret;
}

string WeightView::GetTime()
{
	CTime tm;
	CString str;
	DtCov cov;

	tm = tm.GetCurrentTime();
	str = tm.Format(_T("%Y-%m-%d %H:%M:%S"));

	return cov.wtoc((LPCWSTR)str);
}

void WeightView::HandWeightCancel(int sn)
{
	int n;
	WeightList *wl;

	if (m_WrkStage > STAGE_COMP)
		return;

	n = (int)theWkParam.m_BuyData.WList.size();
	if (sn > 0 && sn <= n)
	{
		CSIntf inf;
		DtReqWeight req;
		DtRspWeight rsp;
		bool ret;

		wl = &theWkParam.m_BuyData.WList[sn - 1];

		if (wl->Status == WS_DONE)
		{
			ShowTips("取消重量");

			req.ScalesId = "123";
			req.BasketNo = wl->SortSn;
			req.RatingTime = wl->SortTime;
			req.LevelCode = wl->LevelCode;
			req.LevelId = wl->LevelId;
			req.WeightTime = wl->WeightTime;
			req.NetWeight = wl->Weight;
			req.Tare = wl->Tare;
			req.SellId = wl->SaleId;
			req.WeightState = "0";

			ret = inf.WeightReport(req, rsp);
			if (ret)
			{
				if (rsp.ErrCode == "0")
				{
					wl->Status = WS_SORT;
					ShowStatus(wl->SortSn, WS_SORT);
				}

				ShowTips(rsp.ErrMsg);
				m_Retry = false;
			}
			else
			{
				ShowTips(inf.GetErr());
				m_Retry = true;
				ShowTips("取消重量失败,按【确认】重试", 1000);
			}
		}
	}
}

void WeightView::AddWeightList(CString &lv)
{
	CString val;
	WeightList wl;
	DtCov cov;

	wl.Status = WS_NONE;
	wl.SortTime = GetTime();
	wl.Tare = "0.0";
	wl.Weight = "0.0";
    wl.WeightTime = wl.SortTime;

	val.Format(_T("%d"), m_TbSn);
	m_TobList.InsertItem(0, val);
	wl.SortSn = cov.wtoc((LPCWSTR)val);

	m_TobList.SetItemText(0, 1, lv);

	wl.LevelCode = cov.wtoc((LPCWSTR)lv);

	// 插入一项数据记录
	theWkParam.m_BuyData.WList.push_back(wl);

	m_TobList.SetFocus();

	int sel;

	sel = m_TobList.GetSelectionMark();
	m_TobList.SetItemState(sel, 0, LVIS_SELECTED);
	m_TobList.SetSelectionMark(-1);

	m_TobList.SetItemState(0, LVIS_FOCUSED | LVIS_SELECTED,LVIS_FOCUSED | LVIS_SELECTED);
	m_TobList.SetSelectionMark(0);

    MsgPost(WM_LEVEL, m_TbSn);

	m_TbSn ++;
}

void WeightView::ShowTips(const char *msg, int sleep)
{
	if (!isRun)
		return;

	CString s;

	s = msg;
    if (sleep != 0)
	{
		Sleep(sleep);
	}

	SetDlgItemText(IDC_STATIC_W_TIPS, s);   
}

void WeightView::ShowTips(string &msg)
{
	if (!isRun)
		return;

	CString s;

	s = msg.c_str();

	SetDlgItemText(IDC_STATIC_W_TIPS, s);   
}

void WeightView::ShowScaleNum(string &n)
{
	CString s;

	s = n.c_str();
	SetDlgItemText(IDC_STATIC_SCALENUM, s);
}

void WeightView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
    
	ShowTime();

	// 获取编程语言列表视图控件的位置和大小  
	CRect rect;
	m_TobList.GetClientRect(&rect);
	m_imgList.Create(32,32,ILC_MASK|ILC_COLOR, 1, 4);

	// 为列表视图控件添加全行选中和栅格风格   
	m_TobList.SetExtendedStyle(LVS_EX_SUBITEMIMAGES | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);   

	m_TobList.InsertColumn(0, _T("序号"), LVCFMT_CENTER, 74, 0);   
	m_TobList.InsertColumn(1, _T("等级"), LVCFMT_CENTER, 56, 0);   
	m_TobList.InsertColumn(2, _T("净重"), LVCFMT_CENTER, 100, 0);   
	m_TobList.InsertColumn(3, _T("毛重"), LVCFMT_CENTER, 100, 0);
	m_TobList.InsertColumn(4, _T("已称重"), LVCFMT_CENTER, 74, 0);   
	m_TobList.InsertColumn(5, _T("删除"), LVCFMT_CENTER, 56, 0);   
	m_TobList.InsertColumn(6, _T("提示"), LVCFMT_CENTER, rect.Width() - 480, 0);

	m_TobList.SetFocus();

	HICON icon;

	icon = AfxGetApp()->LoadIcon(IDI_ICON5);
	m_imgList.Add(icon);
	icon = AfxGetApp()->LoadIcon(IDI_ICON1);
	m_imgList.Add(icon);
	icon = AfxGetApp()->LoadIcon(IDI_ICON3);
	m_imgList.Add(icon);

	m_TobList.SetImageList(&m_imgList,LVSIL_SMALL);//

    m_Thread = AfxBeginThread((AFX_THREADPROC)WorkerThread, this, THREAD_PRIORITY_TIME_CRITICAL);

	SetTimer(1, 1000, NULL);
}

BEGIN_MESSAGE_MAP(WeightView, CFormView)
	ON_WM_TIMER()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST_TOB, &WeightView::OnLvnItemchangedListTob)
	ON_WM_DESTROY()
	ON_NOTIFY(NM_CLICK, IDC_LIST_TOB, &WeightView::OnNMClickListTob)
END_MESSAGE_MAP()


// WeightView 诊断

#ifdef _DEBUG
void WeightView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void WeightView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG

bool WeightView::SqlSaveCont(DtRspCont &cont)
{
    return theSql.InsertCont(cont);
}

bool WeightView::SqlGetCont(DtRspCont &cont)
{
    return theSql.SearchCont(cont);
}

bool WeightView::SqlAddWList(WeightList &wl)
{
	return theSql.InsertWeight(wl);
}

bool WeightView::SqlSetWList(WeightList &wl)
{
	return theSql.UpdataWeight(wl);
}

void WeightView::ShowStatus(string &sortsn, WeightStat s)
{
	int sel;
	LVITEM item;
	CString csn;
	int num;

	csn = sortsn.c_str();

	sel = m_TobList.GetSelectionMark();
	if (csn != m_TobList.GetItemText(sel, 0))
	{
		num = m_TobList.GetItemCount();

		for (int i = 0; i < num; i ++)
		{
			if (m_TobList.GetItemText(i, 0) == csn)
			{
				sel = i;
				break;
			}
		}
	}

	item.mask = LVIF_IMAGE|LVIF_TEXT;
	item.iItem = sel;
	item.pszText = _T(" ");

	if (s == WS_DONE)
	{
		//已称重

		item.iImage = 2;
		item.iSubItem = 4;

		m_TobList.SetItem(&item);
	}
	else if (s == WS_DELETED)
	{
		//删除

		item.iImage = 1;
		item.iSubItem = 5;
		//设置删除标记
		m_TobList.SetItem(&item);
		//清除已称重标记
		item.iImage = 0;
		item.iSubItem = 4;
		item.pszText = _T("");
		m_TobList.SetItem(&item);
		//清除重量
		m_TobList.SetItemText(sel, 2, item.pszText);
		m_TobList.SetItemText(sel, 3, item.pszText);
	}
	else if (s == WS_SORT)
	{
		//恢复

		//清除删除标记
		item.pszText = _T("");
		item.iImage = 0;
		item.iSubItem = 5;
		m_TobList.SetItem(&item);
		//清除已称重标记
		item.iImage = 0;
		item.iSubItem = 4;
		m_TobList.SetItem(&item);
		//清除重量
		m_TobList.SetItemText(sel, 2, item.pszText);
		m_TobList.SetItemText(sel, 3, item.pszText);
	}
}

void WeightView::SetWorkStage(WorkStage ws)
{
    m_WrkStage = ws;
	if (ws > STAGE_WAIT_FARM)
	{
		m_CallEnable = false;
	}
}

EsStatus WeightView::CheckElecScale(ElecScale &es, float td, float t, float tu)
{
	if (!isRun)
		return ESS_NCONNECT;

	CString str = _T(" ");
	CEdit *w;
	EsStatus ess = ESS_OK;

	GetDlgItem(IDC_EDIT_ES, (HWND*)&w);

	if (!es.IsConnected())
	{
		str = "未连接";
		ess = ESS_NCONNECT;
		goto EXIT;
	}

	if (!es.IsZeroDone())
	{
		str = "未归零";
		ess = ESS_NZERO;
		goto EXIT;
	}
	else
	{
		str = "已归零";
	}

	if (t < td || t > tu)
	{
		str = "超皮重";
		ess = ESS_OTARE;
	}

EXIT:

	SetDlgItemText(IDC_EDIT_ES, str);

	return ess;
}

void WeightView::ShowWeight(float nw, float tw)
{
	int sel;
	CString val;
    CString t;

	if (!isRun)
		return;

	val.Format(_T("%.2f"), nw);

    GetDlgItemText(IDC_STATIC_WEIGHT, t);
	if (t != val)
	{
        SetDlgItemText(IDC_STATIC_WEIGHT, val);
	}

	val.Format(_T("%.2f"), tw);

    GetDlgItemText(IDC_STATIC_TARE, t);
	if (t != val)
	{
		SetDlgItemText(IDC_STATIC_TARE, val);
	}

	sel = m_TobList.GetSelectionMark();
	if (sel < 0)
	{
        return;
	}
		
	t = m_TobList.GetItemText(sel, 4);
	if (t != CString(_T(" ")))
	{    
		CString csnw;
		CString csgw;
        
        csnw.Format(_T("%.2f"), nw);
		t = m_TobList.GetItemText(sel, 2);
		if (t != csnw)
		{
            m_TobList.SetItemText(sel, 2, csnw);
		}
		
        csgw.Format(_T("%.2f"), nw + tw);
		t = m_TobList.GetItemText(sel, 3);
		if (t != csgw)
		{
            m_TobList.SetItemText(sel, 3, csgw);
		}
	}
}

void WeightView::ShowWeightOk(string &sortsn, float nw, float tw)
{
	int sel;
	LVITEM item;
	CString csn;
	int num;

	csn = sortsn.c_str();

	sel = m_TobList.GetSelectionMark();
	if (csn != m_TobList.GetItemText(sel, 0))
	{
		num = m_TobList.GetItemCount();

		for (int i = 0; i < num; i ++)
		{
			if (m_TobList.GetItemText(i, 0) == csn)
			{
				sel = i;
				break;
			}
		}
	}

	item.mask = LVIF_IMAGE|LVIF_TEXT;
	item.iItem = sel;
	item.pszText = _T(" ");
	item.iImage = 2;
	item.iSubItem = 4;

	m_TobList.SetItem(&item);

	CString csnw;
	CString csgw;
	CString t;

	csnw.Format(_T("%.2f"), nw);
	t = m_TobList.GetItemText(sel, 2);
	if (t != csnw)
	{
		m_TobList.SetItemText(sel, 2, csnw);
	}

	csgw.Format(_T("%.2f"), nw + tw);
	t = m_TobList.GetItemText(sel, 3);
	if (t != csgw)
	{
		m_TobList.SetItemText(sel, 3, csgw);
	}
}

void WeightView::HandCall(int type)
{
    CSIntf inf;
    bool ret;
	DtRspCall rsp;

    ShowTips("呼叫中...");

	if (type == '1')
	{
		//呼叫
		ret = inf.Call(0, rsp);
	}
	else if (type == '2')
	{
		//重呼
		ret = inf.Call(1, rsp);
	}

    if (ret)
	{
        ShowTips(rsp.ErrMsg.c_str());
	}
	else
	{
		ShowTips("呼叫失败");
	}
}

void WeightView::ShowSortExcep(int sn, string &tip)
{
	CString str;
	CString t;

	str.Format(_T("%d"), sn);

	int n;

	n = m_TobList.GetItemCount();

	for (int i = 0; i < n; i ++)
	{
		t = m_TobList.GetItemText(i, 0);

		if (t == str)
		{
			m_TobList.SetItemText(i, 6, CString(tip.c_str()));
			break;
		}
	}
}

void WeightView::ShowContType(string &c, string &b)
{
	CString type;

	type = c.c_str();
	type += _T(" ");
	type += b.c_str();

	SetDlgItemText(IDC_STATIC_TYPE, type);
}

void WeightView::HandSorting(int sn, int key)
{
    DtRspSort rsp;
    DtReqSort req;
    CSIntf inf;
    bool ret = false;
	WeightList *wl;

	ShowTips("等级验证中");

    wl = &theWkParam.m_BuyData.WList[sn - 1];

    req.LevelCode = wl->LevelCode;
	req.BasketNo = wl->SortSn;
	req.RatingTime = wl->SortTime;
	req.SaleId = GetSaleId();
	req.InspectorCode = "741222";
	req.PoundNoteNum = "1";
	req.PoundNum = theWkParam.m_InitParam.PoundNum;

	ret = inf.SortReport(req, rsp);
	if (ret)
	{
		if (rsp.ErrCode == "0")
		{
            wl->LevelId = rsp.LevelId;
			wl->ContType = rsp.ContractType;
			wl->PackMode = rsp.BundingType;
			wl->Status = WS_SORT;
			wl->SaleId = req.SaleId;

            ShowContType(wl->ContType, wl->PackMode);
			ShowTips("按【结束】键完成定级,按【确认】键磅重");
		}
		else
		{
			wl->Status = WS_EXCEP;
			wl->ExcepTip = rsp.ErrMsg;
            ShowSortExcep(sn, rsp.ErrMsg);
            ShowTips(rsp.ErrMsg);
		}

		SqlAddWList(*wl);

        m_SortLock = false;
		m_Retry = false;
	}
	else
	{
        ShowTips("通信失败");
		m_Retry = true;
		ShowTips("定级失败,按【确认】重试", 1000);
	}
}

void WeightView::HandWeightDel(int sn, int sel)
{
	int n;

	n = (int)theWkParam.m_BuyData.WList.size();
	if (sn > 0 && sn <= n)
	{
        WeightList *wl;
		CSIntf inf;
		DtReqWeight req;
		DtRspWeight rsp;
		bool ret;

        wl = &theWkParam.m_BuyData.WList[sn - 1];

		DtCov cov;
        WeightStat status;
		
		req.ScalesId = "123";
		req.BasketNo = wl->SortSn;
		req.RatingTime = wl->SortTime;
		req.LevelCode = wl->LevelCode;
		req.LevelId = wl->LevelId;
		req.WeightTime = wl->WeightTime;
		req.NetWeight = wl->Weight;
		req.Tare = wl->Tare;
		req.SellId = wl->SaleId;

		if (wl->Status == WS_DONE || wl->Status == WS_SORT)
		{
			//删除
            ShowTips("按【删除】恢复/取消烟框");

            req.WeightState = "2";
			status = WS_DELETED;
		}
		else if (wl->Status == WS_DELETED)
		{
			ShowTips("恢复烟框");
			req.WeightState = "0";
			status = WS_SORT;		
		}

		ret = inf.WeightReport(req, rsp);
		if (ret)
		{
			if (rsp.ErrCode == "0")
			{
				wl->Status = status;
				ShowStatus(wl->SortSn, status);
				SqlSetWList(*wl);
			}

			ShowTips(rsp.ErrMsg);
			m_Retry = false;
		}
		else
		{
			ShowTips(inf.GetErr());
			m_Retry = true;
			ShowTips("删除/恢复失败,按【确认】重试", 1000);
		}
	}
}

void WeightView::HandleMessages(MSG &msg, int &stage)
{
    
    switch (msg.message)
	{
	case WM_LEVEL:
	{
        HandSorting((int)msg.wParam, (int)msg.lParam);
	}
	break;
	case WM_WEIGHT_DEL:
	{
        HandWeightDel((int)msg.wParam, (int)msg.lParam);
	}
	break;
	case WM_WEIGHT_FINISH:
	{
		WListStatus wls;

		wls = theWkParam.CheckWList();
		if (wls == WLS_NW || wls == WLS_W)
		{
			//仅定级完成
			PlayTips(ST_SORT_DONE);
			stage = STAGE_ONLY_SORT;
			ShowTips("定级结束");
		}
		else if (wls == WLS_WD)
		{
			PlayTips(ST_FARM_CARD_START);
			stage = STAGE_COMP;
		}
		else if (wls == WLS_DLE)
		{
			int ret;

			ret = MessageBox( L"按【确认】键取消交售, 按【取消】键继续交售", L"是否取消该烟农交烟?", MB_OKCANCEL);
			if (ret != 1)
			{
				break;
			}

			PlayTips(ST_DONE);

			stage = STAGE_INIT;
		}
        //HandEndSort();
	}
	break;
	case WM_CALL:
	{
        HandCall((int)msg.wParam);
	}
	break;
	case WM_ELECSCALE_ZERO:
	{
        PlayTips(ST_SCALE_ZERO);
	}	
	break;
    case WM_SAVE:
	{
        if (OperatorConfirm())
		{
			stage = STAGE_FINAL;
		}
	}
	break;
	case WM_RESTART:
	{
        stage = STAGE_INIT;
	}
	break;
	case WM_CONT_DEL:
	{
        if (HandContDel())
		{
            stage = STAGE_INIT;
		}
	}
	break;
	case WM_WEIGHT_CANCEL:
	{
        HandWeightCancel((int)msg.wParam);
	}
	break;
	}
}

bool WeightView::FarmerConfirm(string &cardid)
{
    CSIntf inf;
	bool ret;
    DtReqConfirm req;
	DtRspConfirm rsp;

	ShowTips("烟农确认");

	req.BankCardNum = cardid;
	req.SellId = GetSaleId();
    theWkParam.GetValid(req.ValidTotalbaskets, req.ValidWeight);

	ret = inf.Confirm(req, rsp);
    if (ret)
	{
        ret = (rsp.ErrCode == "0");
		ShowTips(rsp.ErrMsg);
	}
	else
	{
		ShowTips(inf.GetErr());
		ShowTips("烟农确认失败,正在重试", 1000);
	}

	return ret;
}

bool WeightView::OperatorConfirm()
{
	CSIntf inf;
	bool ret;

	DtReqOpConfirm req; 
	DtRspOpConfirm rsp;

	req.BankCardNum = theWkParam.m_ContInfo.BankCardNum;
	req.SellId = GetSaleId();

	ShowTips("保存数据");

	ret = inf.OperatorConfirm(req, rsp);
	if (ret)
	{
        ret = (rsp.ErrCode == "0");
		ShowTips(rsp.ErrMsg);
		m_Retry = false;
	}
	else
	{
		ShowTips(inf.GetErr());
		m_Retry = true;
		ShowTips("保存失败,按【确认】重试", 1000);
	}

	return ret;
}

void WeightView::HandEndSort()
{
    CSIntf inf;
    DtReqEndSort req;
    DtRspEndSort rsp;
    bool ret;

	req.SaleId = GetSaleId();
	req.BasketCount = m_TbSn - 1;

	ret = inf.EndSort(req, rsp);

}

void WeightView::ClearDisplay()
{
	CString s = _T("");

	SetDlgItemText(IDC_STATIC_CONTNUM, s);
	SetDlgItemText(IDC_STATIC_NAME, s);
	SetDlgItemText(IDC_STATIC_TOTAL, s);
	SetDlgItemText(IDC_STATIC_REMAIN, s);
	SetDlgItemText(IDC_STATIC_TODAY, s);
	SetDlgItemText(IDC_STATIC_INLAND, s);
	SetDlgItemText(IDC_STATIC_EXPORT, s);
	SetDlgItemText(IDC_STATIC_TYPE, s);

	m_TobList.DeleteAllItems();
}

string WeightView::GetSaleId()
{
    return theWkParam.m_ContInfo.SaleId;
}

WeightStat WeightView::HandWeightOk(DtReqWeight &req, int sn, float nw, float tw)
{
    WeightStat ws = WS_NONE;
    CSIntf inf;
    DtRspWeight rsp;
    bool ret;
    WeightList *wl;
	int n;

    n = (int)theWkParam.m_BuyData.WList.size();
    if (sn <= n)
	{
		wl = &theWkParam.m_BuyData.WList[sn - 1];

		if (wl->Status == WS_SORT)
		{
			DtCov cov;
            
            ShowTips("磅重验证");

			wl->WeightTime = GetTime();
            wl->Tare = cov.ftos(tw);
            wl->Weight = cov.ftos(nw);

			req.WeightState = "1";
			req.ScalesId = "123";
            req.BasketNo = wl->SortSn;
			req.RatingTime = wl->SortTime;
			req.LevelCode = wl->LevelCode;
			req.LevelId = wl->LevelId;
            req.WeightTime = wl->WeightTime;
			req.NetWeight = wl->Weight;
			req.Tare = wl->Tare;
			req.SellId = wl->SaleId;

            theSndPlay.PlayWeight(wl->LevelCode, nw);
			
			ret = inf.WeightReport(req, rsp);
			if (ret)
			{
                if (rsp.ErrCode == "0")
				{
					wl->Status = WS_DONE;
					ShowWeightOk(wl->SortSn, nw, tw);
				}

				ShowTips(rsp.ErrMsg);
				m_Retry = false;
				ws = WS_SORT;
			}
			else
			{
				ShowTips(inf.GetErr());
				m_Retry = true;
                ShowTips("磅重失败,按【确认】重试", 1000);
			}
		}
	}

	return ws;
}

bool WeightView::GetContract(string &cardid, DtRspCont &rsp)
{
    bool ret = false;
    CSIntf inf;

	ShowTips("下载合同");

	ret = inf.GetContract(cardid, rsp);
	if (!ret)
	{
		ShowTips(inf.GetErr());
		ShowTips("下载合同失败,正在重试", 1000);
	}

	return ret;
}

bool WeightView::CheckCont(DtRspCont &ci)
{
	bool ret = false;

	if (ci.ErrCode == "0")
	{
		ret = true;
	}

    ShowTips(ci.ErrMsg);

	return ret;
}

void WeightView::ShowCont(DtRspCont &ci)
{
	CString s; 

	s = ci.BankCardNum.c_str();
	SetDlgItemText(IDC_STATIC_CONTNUM, s);

	s = ci.FarmerName.c_str();
	SetDlgItemText(IDC_STATIC_NAME, s);

	s = ci.ContractAmount.c_str();
	SetDlgItemText(IDC_STATIC_TOTAL, s);

	s = ci.ContractMargin.c_str();
	SetDlgItemText(IDC_STATIC_REMAIN, s);

	s = ci.CurrentMargin.c_str();
	SetDlgItemText(IDC_STATIC_TODAY, s);
}

void WeightView::ReInit()
{
    m_SortLock = false;
	m_TbSn = 1;

    m_CallDelay = 5;
	m_CallEnable = true;
    m_ContLock = false;
	m_Retry = false;

	m_ItemClr = -2;

	ClearDisplay();
}

void WeightView::CheckCallDelay()
{
	if (m_WrkStage <= STAGE_WAIT_FARM)
	{
		if (!m_CallEnable)
		{
			if (m_CallDelay == 0)
			{
				m_CallEnable = true;
				m_CallDelay = 5;
			}
			else
			{
				m_CallDelay --;
			}
		}
	}
}

void WeightView::PlayTips(SoundTips st)
{
	WCHAR *tip;

	switch (st)
	{
	case ST_BEGIN:
		tip = _T("开始交售");
		break;
	case ST_SORT_DONE:
		tip = _T("定级完成");
		break;
	case ST_FARM_CARD_START:
		tip = _T("烟农请刷卡");
		break;
	case ST_FARM_CARD_END:
		tip = _T("烟农已刷卡");
		break;
	case ST_DONE:
		tip = _T("交售完成");
		break;
	case ST_SCALE_NZERO:
		tip = _T("秤未归零");
		break;
	case ST_SCALE_ZERO:
		tip = _T("秤已归零");
		break;
	case ST_WOVER_TODAY:
		tip = _T("超今日交售量");
		break;
	case ST_WOVER_CONT:
		tip = _T("交售超合同");
		break;
	default:
		return;
	}

	theSndPlay.PlayTips(tip);
}

void WeightView::SetZero(ElecScale &es)
{
	int sec;
	int devi;

	theWkParam.GetZeroParam(sec, devi);
	es.StartZeroCheck(sec, (float)devi);
}

DWORD WeightView::WorkerThread(LPVOID lParam)
{
    WeightView *dlg;
	int stage = 0;
    MSG msg;
    string cardid;
	RfCardReader creader;
    ElecScale scale;
	float nweight = 0;
	float tweight = 0;
	EsStatus ess;
	float systare = 0;
    float taredown = 0;
	float tareup = 0;
	int carderr = 0;

	dlg = (WeightView *)lParam;
	dlg->isRun = true;

    AfxSocketInit();
	Sleep(100);

	creader.Open();
    dlg->ShowScaleNum(theWkParam.m_InitParam.PoundNum);

	scale.setMsgReceiver(dlg->m_Thread->m_nThreadID);
	scale.startWeight(500);
	scale.EnableMsg(ES_MSG_ZERO);

	theWkParam.GetTareParam(systare, taredown, tareup);

	dlg->SetZero(scale);

	while (dlg->isRun)
	{
        scale.GetWeight(nweight, tweight);
		ess = dlg->CheckElecScale(scale, taredown, tweight, tareup);

		nweight -= systare;
		tweight += systare;

		dlg->ShowWeight(nweight, tweight);

		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (dlg->m_Retry && dlg->m_LastMsg.message != msg.message)
			{
                dlg->ShowTips("有未处理完的事务");
				continue;
			}

			dlg->m_LastMsg = msg;

			switch (msg.message)
			{
			case WM_WEIGHT_OK:
			{
				if (nweight <= 0)
					break;

                DtReqWeight req;
				if (ess != ESS_OK)
				{
					if (ess == ESS_NZERO)
					{
						dlg->PlayTips(ST_SCALE_NZERO);
					}

					break;
				}

                if (dlg->HandWeightOk(req, (int)msg.wParam, nweight, tweight) == WS_SORT)
				{
					dlg->SetZero(scale);
				}
			}
			break;
			case WM_RETURN:
			    goto EXIT;
			break;
			default:
                dlg->HandleMessages(msg, stage);
            break;
			}  
		}

		switch (stage)
		{
		case STAGE_INIT:
		{
			dlg->ReInit();

            stage ++;
			dlg->SetWorkStage(STAGE_WAIT_FARM);
		    stage = STAGE_RESTORE;//TODO 测试
		}
		break;
		case STAGE_WAIT_FARM: //等待烟农报道
		{
            int err;

            Sleep(500);

			if (!dlg->ReadCard(creader, cardid, &err))
			{
                if (err == -1)
				{
					carderr ++;
					if (carderr > 3)
					{
                        dlg->ShowTips("未发现读卡器");
					}
				}
				else
				{
					carderr = 0;
					dlg->ShowTips("等待烟农刷卡,按【返回】键进入功能菜单");
				}

				break;
			}

			dlg->ClearData();
			stage ++;
		}
		break;
		case STAGE_QUERY_ORDER: //下载订单信息
		{
			if (!dlg->GetContract(cardid, theWkParam.m_ContInfo))
			{
				Sleep(1000);
				break;
			}

			if (!dlg->CheckCont(theWkParam.m_ContInfo))
			{
				stage = STAGE_INIT;
				Sleep(2000);
				break;
			}

			dlg->ShowCont(theWkParam.m_ContInfo);
            dlg->SqlSaveCont(theWkParam.m_ContInfo);

			stage ++;
            dlg->SetWorkStage(STAGE_READY);
			dlg->ShowTips("等待定级磅码, 按【结束】键取消交售");
			dlg->PlayTips(ST_BEGIN);
		}
		break;
		case STAGE_READY: //业务进行中
		{
				
		}
		break;
		case STAGE_ONLY_SORT:
		{
			dlg->SetWorkStage(STAGE_ONLY_SORT);
		}
		break;
		case STAGE_COMP:
		{
			stage ++;
			dlg->SetWorkStage(STAGE_WAIT_CONFIRM);
			
		}
		break;
		case STAGE_WAIT_CONFIRM: //等待烟农刷卡
		{
			int err;

            Sleep(500);

			if (!dlg->ReadCard(creader, cardid, &err))
			{
				if (err == -1)
				{
					dlg->ShowTips("未发现读卡器");
				}
				else
				{
                    dlg->ShowTips("定级、磅重已完成,请烟农刷卡确认");
				}

				break;
			}

			if (cardid == theWkParam.m_ContInfo.BankCardNum)
			{
				dlg->PlayTips(ST_FARM_CARD_END);
				stage ++;
			}
			else
			{
				string tip;

				tip = "卡号异常:";
				tip += cardid;
				tip += ",请重新刷卡";
				dlg->ShowTips(tip);
				Sleep(2000);
			}
		}
		break;
		case STAGE_SEND_CONFIRM: //发送确认消息
		{
			if (dlg->FarmerConfirm(cardid))
			{
                stage ++;
                dlg->SetWorkStage(STAGE_WAIT_SAVE);
			}
		}
		break;
		case STAGE_WAIT_SAVE:
		{
			dlg->ShowTips("按【保存】键提交数据");
		}
		break;
		case STAGE_FINAL:
		{
			dlg->ShowTips("数据已保存,按【确认】键结束本次交售");
			dlg->SetWorkStage(STAGE_FINAL);
		}
		break;
		case STAGE_RESTORE:
		{
			dlg->SqlGetCont(theWkParam.GetCont());
            dlg->Restore();
			stage = 1;//TODO
		}
		break;
		}

		Sleep(100);
	}

EXIT:
	creader.Close();
	scale.endWeight();
	theWkParam.Clear();

	if (dlg->isRun)
	{
		dlg->isRun = false;
		dlg->SwitchView();
	}

	return 0;
}

int WeightView::Restore()
{
    CSIntf inf;
    DtReqRestore req;
    DtRspRestore rsp;
	bool ret;

	ret = inf.Restore(req, rsp);
	if (!ret)
		return 0;

	if (rsp.ErrCode == "0")
	{

	}

	return 0;
}

bool WeightView::ReadCard(RfCardReader &reader, string &cardid, int *err)
{
	RfidInfo inf;
    int e;
	bool ret = false;

	reader.Lock();

	e = reader.CardScan(&inf);
	if (err != NULL)
	{
        *err = e;
	}

	if (e != 1)
		goto EXIT;

	ret = reader.CreditCardRead(cardid);

EXIT:
    reader.Unlock();

	return ret;
}

void WeightView::SwitchView()
{
	CMainFrame* pmf = (CMainFrame*)(AfxGetApp()->m_pMainWnd);

	pmf->SwitchView(IDD_VIEW_FUCSEL);
}

void WeightView::ShowTime()
{
	CTime tm;
	CString str;

	tm = tm.GetCurrentTime();
	str = tm.Format(_T("%Y-%m-%d %H:%M:%S"));
	SetDlgItemText(IDC_STATIC_TIME, str);
}

// WeightView 消息处理程序
void WeightView::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (nIDEvent == 1)
	{
        ShowTime();
		CheckCallDelay();
	}
}

void WeightView::OnLvnItemchangedListTob(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码

	if (pNMLV->uChanged == LVIF_STATE)
	{
		if (pNMLV->uNewState == 0 && pNMLV->uOldState == 1)
		{
			//检查旧行是否已经称重
			CString t;

			t = m_TobList.GetItemText(pNMLV->iItem, 4);
			if (t != CString(_T(" ")))
			{
				//清空单元格里的重量
                m_ItemClr = pNMLV->iItem;
				m_TobList.SetItemText(pNMLV->iItem, 2, CString(_T(" ")));
				m_TobList.SetItemText(pNMLV->iItem, 3, CString(_T(" ")));
			}
		}
	}

	*pResult = 0;
}

void WeightView::OnDestroy()
{
	Exit();

	CFormView::OnDestroy();

	// TODO: 在此处添加消息处理程序代码
}

void WeightView::OnNMClickListTob(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_ItemClr >= 0)
	{
		m_TobList.SetItemText(m_ItemClr, 2, _T(" "));
		m_TobList.SetItemText(m_ItemClr, 3, _T(" "));
        m_ItemClr = -2;
	}

	*pResult = 0;
}
