//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by its2015.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_ITSM_FORM                   101
#define IDD_STARTVIEW                   101
#define IDD_VIEW_START                  101
#define IDR_MAINFRAME                   128
#define IDR_ITSMTYPE                    129
#define IDD_DIALOG_FUCSEL               131
#define IDD_VIEW_FUCSEL                 131
#define IDD_DIALOG1                     132
#define IDD_DIALOG2                     133
#define IDD_DIALOG_SERVERIP             133
#define IDD_DIALOG_WEIGHT               134
#define IDD_WEIGHTVIEW                  134
#define IDD_VIEW_WEIGHT                 134
#define IDD_DIALOG_SORT                 135
#define IDI_ICON1                       136
#define IDI_ICON2                       140
#define IDI_ICON3                       141
#define IDI_ICON4                       142
#define IDI_ICON5                       143
#define IDD_DIALOG3                     144
#define IDD_DIALOG_DEVSET               144
#define IDD_VIEW_DEVSET                 144
#define IDD_DIALOG4                     145
#define IDD_DIALOG_COMCFG               145
#define IDD_DIALOG5                     146
#define IDD_DIALOG_SEL                  146
#define IDD_DIALOG_RESTOR               147
#define IDD_DIALOG_RESTORE              147
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON3                     1002
#define IDC_STATIC_CHECK                1003
#define IDC_BUTTON4                     1003
#define IDC_STATIC_SORTING              1004
#define IDC_STATIC_WEIGHT               1005
#define IDC_STATIC_LOCALIP              1006
#define IDC_STATIC_REMOTEIP             1007
#define IDC_STATIC_DEVID                1008
#define IDC_STATIC_DEVSN                1009
#define IDC_STATIC_TIPS                 1010
#define IDC_STATIC_DEVSN2               1011
#define IDC_STATIC_SWVER                1011
#define IDC_EDIT1                       1012
#define IDC_EDIT2                       1013
#define IDC_LIST_TOB                    1013
#define IDC_EDIT_TBINPUT                1014
#define IDC_EDIT4                       1015
#define IDC_STATIC_TIME                 1016
#define IDC_STATIC_TOTAL                1017
#define IDC_STATIC_REMAIN               1018
#define IDC_STATIC_TODAY                1019
#define IDC_STATIC_INLAND               1020
#define IDC_STATIC_EXPORT               1021
#define IDC_STATIC_TARE                 1022
#define IDC_STATIC_W_TIPS               1023
#define IDC_STATIC_NAME                 1025
#define IDC_STATIC_CONTNUM              1026
#define IDC_EDIT_ES                     1028
#define IDC_STATIC_TYPE                 1029
#define IDC_STATIC_SCALENUM             1031
#define IDC_STATIC_S_TIPS               1033
#define IDC_COMBO1                      1036
#define IDC_COMBO2                      1037
#define IDC_LIST1                       1037
#define ID_32771                        32771
#define ID_32772                        32772
#define IDS_NEW                         65000
#define IDS_FILE                        65001
#define IDS_HELP                        65002
#define IDS_SAVE                        65003
#define IDS_CUT                         65004
#define IDS_COPY                        65005
#define IDS_PASTE                       65006
#define IDS_ABOUT                       65007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32773
#define _APS_NEXT_CONTROL_VALUE         1038
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
