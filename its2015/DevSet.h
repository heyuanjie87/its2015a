#pragma once



// DevSet 窗体视图

class DevSet : public CFormView
{
	DECLARE_DYNCREATE(DevSet)

public:
	DevSet();           // 动态创建所使用的受保护的构造函数
	virtual ~DevSet();

public:
	enum { IDD = IDD_VIEW_DEVSET };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	BOOL PreTranslateMessage(MSG *pMsg);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
};


