// MainFrm.cpp : CMainFrame 类的实现
//

#include "stdafx.h"
#include "its2015.h"

#include "MainFrm.h"
#include "StartView.h"
#include "WeightView.h"
#include "ComCfg.h"
#include "FucSel.h"
#include "DevSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_MESSAGE(WM_SWVIEW, &CMainFrame::OnSwitchView)
    ON_MESSAGE(WM_CLOSE, &CMainFrame::OnCloseWin)
	ON_COMMAND(ID_32772, &CMainFrame::OnServerIp)
	ON_COMMAND(ID_32771, &CMainFrame::OnComSet)
END_MESSAGE_MAP()


// CMainFrame 构造/析构

CMainFrame::CMainFrame()
{
	// TODO: 在此添加成员初始化代码
}

CMainFrame::~CMainFrame()
{
}


BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式
	cs.cx = 800;
	cs.cy = 520;
	cs.style &= ~WS_THICKFRAME;
	cs.style &= ~WS_MAXIMIZEBOX;
    cs.style &= ~FWS_ADDTOTITLE;

	return TRUE;
}

LRESULT CMainFrame::OnCloseWin(WPARAM wParam, LPARAM lParam)
{
    if(IDYES == AfxMessageBox(_T("是否关闭程序?"), MB_YESNO))
	{
		CFrameWnd::OnClose();
	}

	return NULL;
}

// CMainFrame 诊断

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

void CMainFrame::SwitchView(int id)
{
    SendMessage(WM_SWVIEW, id, 0);
}

// CMainFrame 消息处理程序
LRESULT CMainFrame::OnSwitchView(WPARAM wParam, LPARAM lParam)
{
	CView *pOldActiveView;
	CCreateContext text;   

	switch (wParam)
	{
	case IDD_VIEW_WEIGHT:
		NextView = new WeightView();
		break;
	case IDD_VIEW_START:
        NextView = new StartView();
		break;
	case IDD_VIEW_FUCSEL:
        NextView = new FucSel();
	    break;
	case IDD_VIEW_DEVSET:
		NextView = new DevSet();
		break;
	default:
		return NULL;
	}

	pOldActiveView = GetActiveView();
	text.m_pCurrentDoc = pOldActiveView->GetDocument();

	NextView->Create(NULL, NULL, WS_BORDER|WS_CHILD, CFrameWnd::rectDefault, this, (UINT)wParam, &text);
	NextView->OnInitialUpdate();
	SetActiveView(NextView);
	NextView->ShowWindow(SW_SHOW);
	delete pOldActiveView;
	NextView->SetDlgCtrlID(AFX_IDW_PANE_FIRST);
	RecalcLayout();

	return NULL;
}

#include "ServerIp.h"

void CMainFrame::OnServerIp()
{
	// TODO: 在此添加命令处理程序代码
	ServerIp dlg;

	if (IDOK == dlg.DoModal())
	{
        CView *view;
        
		view = GetActiveView();
		view->SendMessage(WM_CFG_CHANGED, 0, 0);
	}
}

void CMainFrame::OnComSet()
{
	// TODO: 在此添加命令处理程序代码
	ComCfg dlg;

	dlg.DoModal();
}
