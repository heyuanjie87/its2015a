#pragma once


// ServerIp 对话框

class ServerIp : public CDialog
{
	DECLARE_DYNAMIC(ServerIp)

public:
	ServerIp(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~ServerIp();

// 对话框数据
	enum { IDD = IDD_DIALOG_SERVERIP };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	CString m_Ip;
    CString m_Port;

	afx_msg void OnBnClickedOk();
	
};
