// its2015View.cpp : Cits2015View 类的实现
//

#include "stdafx.h"
#include "its2015.h"

#include "its2015Doc.h"
#include "its2015View.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cits2015View

IMPLEMENT_DYNCREATE(Cits2015View, CView)

BEGIN_MESSAGE_MAP(Cits2015View, CView)
END_MESSAGE_MAP()

// Cits2015View 构造/析构

Cits2015View::Cits2015View()
{
	// TODO: 在此处添加构造代码

}

Cits2015View::~Cits2015View()
{
}

BOOL Cits2015View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// Cits2015View 绘制

void Cits2015View::OnDraw(CDC* /*pDC*/)
{
	Cits2015Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
}


// Cits2015View 诊断

#ifdef _DEBUG
void Cits2015View::AssertValid() const
{
	CView::AssertValid();
}

void Cits2015View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

Cits2015Doc* Cits2015View::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(Cits2015Doc)));
	return (Cits2015Doc*)m_pDocument;
}
#endif //_DEBUG


// Cits2015View 消息处理程序
