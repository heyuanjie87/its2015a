// LevelInput.cpp : 实现文件
//

#include "stdafx.h"
#include "its2015.h"
#include "LevelInput.h"


// LevelInput 对话框

IMPLEMENT_DYNAMIC(LevelInput, CDialog)

LevelInput::LevelInput(CWnd* pParent /*=NULL*/)
	: CDialog(LevelInput::IDD, pParent)
	, m_Level(_T(""))
{
}

LevelInput::~LevelInput()
{
}

BOOL LevelInput::OnInitDialog()
{
    CDialog::OnInitDialog();

	m_Level = m_InputEdit.Get();
	UpdateData(FALSE);

	return TRUE;
}

bool LevelInput::IsDone()
{
	return m_InputEdit.IsDone();
}

INT_PTR LevelInput::Start(int key)
{
	m_Level = "";
    m_InputEdit.Clear();
    m_InputEdit.Input(key);

	return CDialog::DoModal();
}

CString LevelInput::GetLevel(bool clr)
{
	if (clr)
        m_InputEdit.Clear();

    return m_Level;
}

void LevelInput::OnOK()
{
    UpdateData(TRUE);

	if (IsDone())
	{
        CDialog::OnOK();
	}
}

void LevelInput::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_InputEdit);
	DDX_Text(pDX, IDC_EDIT1, m_Level);
}

BEGIN_MESSAGE_MAP(LevelInput, CDialog)
END_MESSAGE_MAP()


// LevelInput 消息处理程序
