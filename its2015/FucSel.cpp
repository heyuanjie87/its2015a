// FucSel.cpp : 实现文件
//

#include "stdafx.h"
#include "its2015.h"
#include "FucSel.h"
#include "MainFrm.h"


// FucSel

IMPLEMENT_DYNCREATE(FucSel, CFormView)

FucSel::FucSel()
	: CFormView(FucSel::IDD)
{

}

FucSel::~FucSel()
{
}

void FucSel::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

BOOL FucSel::PreTranslateMessage(MSG *pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		if (pMsg->wParam == VK_HOME)
		{
			CMainFrame* pmf = (CMainFrame*)(AfxGetApp()->m_pMainWnd);

			pmf->SwitchView(IDD_VIEW_START);

			return TRUE;
		}

		if (pMsg->wParam == '1')
		{
			OnBnClickedButton3();

			return TRUE;
		}
		else if (pMsg->wParam == '2')
		{
			OnBnClickedButton2();

			return TRUE;
		}

		if (pMsg->wParam == VK_RETURN)
		{
			return TRUE;
		}
	}

	return CFormView::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(FucSel, CFormView)
	ON_BN_CLICKED(IDC_BUTTON3, &FucSel::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON2, &FucSel::OnBnClickedButton2)
END_MESSAGE_MAP()


// FucSel 诊断

#ifdef _DEBUG
void FucSel::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void FucSel::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// FucSel 消息处理程序

void FucSel::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	CMainFrame* pmf = (CMainFrame*)(AfxGetApp()->m_pMainWnd);

	pmf->SwitchView(IDD_DIALOG_WEIGHT);
}

void FucSel::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	CMainFrame* pmf = (CMainFrame*)(AfxGetApp()->m_pMainWnd);

	pmf->SwitchView(IDD_DIALOG_DEVSET);
}
