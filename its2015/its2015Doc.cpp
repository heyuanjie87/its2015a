// its2015Doc.cpp : Cits2015Doc 类的实现
//

#include "stdafx.h"
#include "its2015.h"

#include "its2015Doc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cits2015Doc

IMPLEMENT_DYNCREATE(Cits2015Doc, CDocument)

BEGIN_MESSAGE_MAP(Cits2015Doc, CDocument)
END_MESSAGE_MAP()


// Cits2015Doc 构造/析构

Cits2015Doc::Cits2015Doc()
{
	// TODO: 在此添加一次性构造代码

}

Cits2015Doc::~Cits2015Doc()
{
}

BOOL Cits2015Doc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: 在此添加重新初始化代码
	// (SDI 文档将重用该文档)

	return TRUE;
}




// Cits2015Doc 序列化

void Cits2015Doc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: 在此添加存储代码
	}
	else
	{
		// TODO: 在此添加加载代码
	}
}


// Cits2015Doc 诊断

#ifdef _DEBUG
void Cits2015Doc::AssertValid() const
{
	CDocument::AssertValid();
}

void Cits2015Doc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// Cits2015Doc 命令
