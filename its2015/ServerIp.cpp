// ServerIp.cpp : 实现文件
//

#include "stdafx.h"
#include "its2015.h"
#include "ServerIp.h"

#include "DevConfig.h"


// ServerIp 对话框

IMPLEMENT_DYNAMIC(ServerIp, CDialog)

ServerIp::ServerIp(CWnd* pParent /*=NULL*/)
	: CDialog(ServerIp::IDD, pParent)
	, m_Ip(_T(""))
	, m_Port(_T(""))
{

}

ServerIp::~ServerIp()
{
}

void ServerIp::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_Ip);
	DDX_Text(pDX, IDC_EDIT2, m_Port);
}

BOOL ServerIp::OnInitDialog()
{
	CDialog::OnInitDialog();

 	WCHAR ip[32];
	WCHAR port[32];
	DevConfig cfg;

	cfg.GetServerIp(ip, port);
    m_Ip = ip;
    m_Port = port;

	UpdateData(FALSE);

	return TRUE;
}


BEGIN_MESSAGE_MAP(ServerIp, CDialog)
	ON_BN_CLICKED(IDOK, &ServerIp::OnBnClickedOk)
END_MESSAGE_MAP()


// ServerIp 消息处理程序

void ServerIp::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
    DevConfig cfg;
 
    UpdateData(TRUE);

	if (!cfg.IsIP(m_Ip))
	{
		SetDlgItemText(IDC_STATIC_S_TIPS, _T("IP地址错误,请重新输入"));
	}
	else if (m_Port.IsEmpty())
	{
        SetDlgItemText(IDC_STATIC_S_TIPS, _T("端口不能为空"));
	}
	else
	{
		cfg.SetServerIp((LPCWSTR)m_Ip, (LPCWSTR)m_Port);

		OnOK();		
	}
}
