// its2015Doc.h : Cits2015Doc 类的接口
//


#pragma once


class Cits2015Doc : public CDocument
{
protected: // 仅从序列化创建
	Cits2015Doc();
	DECLARE_DYNCREATE(Cits2015Doc)

// 属性
public:

// 操作
public:

// 重写
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// 实现
public:
	virtual ~Cits2015Doc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	DECLARE_MESSAGE_MAP()
};


